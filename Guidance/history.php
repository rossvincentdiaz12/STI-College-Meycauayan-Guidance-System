<?php
session_start();

	if(isset($_SESSION["username"])==false)// ginagamit yan para di ka makapunta sa 2nd page, need to log in first para makapunta sa next page.
	{
		header("location:index.php");
	}
?>
<!DOCTYPE html>
<html>
<title>STI History</title>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Untitled</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Cookie">
    <link rel="stylesheet" href="assets/fonts/font-awesome.min.css">
    <link rel="stylesheet" href="assets/fonts/material-icons.css">
    <link rel="stylesheet" href="assets/css/styles.css">
    <link rel="stylesheet" href="assets/css/Pretty-Footer.css">
</head>

<body>
    <nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header"><a href="history.php" class="navbar-brand navbar-link"><strong>STI College Meycauayan</strong>   History</a>
            <button data-toggle="collapse" data-target="#navcol-1" class="navbar-toggle collapsed"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
        </div>

        <div class="collapse navbar-collapse" id="navcol-1">
            
                
                <ul class="nav navbar-nav navbar-right">
                <li role="presentation"><a href="indexAnnounce.php">Announcements </a></li>
                <li class="dropdown"><a data-toggle="dropdown" aria-expanded="false" href="#" class="dropdown-toggle">Upload CSV File<span class="caret"></span></a>

                    <ul role="menu" class="dropdown-menu">
                        <li role="presentation"><a href="tertiarycsv.php">Tertiary Students</a></li>
                        <li role="presentation"><a href="seniorcsv.php">Senior High Students</a></li>
                        
                    </ul>
                </li>
                
                <li class="dropdown"><a data-toggle="dropdown" aria-expanded="false" href="#" class="dropdown-toggle">Grades <span class="caret"></span></a>
                    <ul role="menu" class="dropdown-menu">
                        <li role="presentation"><a href="indexgradter.php">Tertiary Students</a></li>
                        <li role="presentation"><a href="indexgradshs.php">Senior High Students</a></li>
                    </ul>

                    <li class="dropdown"><a data-toggle="dropdown" aria-expanded="false" href="#" class="dropdown-toggle">Offenses <span class="caret"></span></a>
                <ul role="menu" class="dropdown-menu">
                    <li role="presentation"><a href="indexteroff.php">Tertiary Students</a></li>
                    <li role="presentation"><a href="indexshsoff.php">Senior High Students</a></li>
                </ul>

                <li class="dropdown"><a data-toggle="dropdown" aria-expanded="false" href="#" class="dropdown-toggle">About STI<span class="caret"></span></a>
                    <ul role="menu" class="dropdown-menu">
                        <li role="presentation"><a href="history.php">History</a></li>
                        <li role="presentation"><a href="vision.php">Vision, Mission and Hymn</a></li>
                    </ul>
                </li>

                <li class="dropdown"><a data-toggle="dropdown" aria-expanded="false" href="#" class="dropdown-toggle">
                	<?php echo $_SESSION['username']; ?><span class="caret"></span></a>
                    <ul role="menu" class="dropdown-menu">
                        <li role="presentation"><a href="updateadmin.php">Edit Profile</a></li>
                        <li role="presentation"><a href="addusers.php">Add Users</a></li>
                        <li role="presentation"><a href="addoffenses.php">Add Offenses</a></li>
                        <li role="presentation"><a href="index.php?action=logout">Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav><br><br><br>
    <div>
        <div class="thumbnail"> <img src="assets/img/sticollege.jpg"></div>
    </div>
    <div class="container sti">
        <h1>   STI History</h1>
        <p>From its humble beginnings on August 21, 1983 as a computer training center with only two campuses, STI now has campuses all over the Philippines and has diversified into ICT-enhanced programs in Information Technology, Business and Management,
            Tourism and Hospitality Management, Engineering, and Arts and Sciences.</p>
        <p>It all started when four visionaries conceptualized setting up a training center to fill very specific manpower needs. </p>
        <p>It was in the early ‘80s when Augusto C. Lagman, Herman T. Gamboa, Benjamin A. Santos, and Edgar H. Sarte — four entrepreneurs came together to set up Systems Technology Institute (STI), a training center that delivers basic programming education
            to professionals and students who want to learn this new skill.</p>
        <p>Systems Technology Institute’s name came from countless brainstorming sessions among the founders, perhaps from Sarte’s penchant for three-letter acronyms from the companies he managed at the time.</p>
        <p>The first two schools were inaugurated in August 21, 1983 in Buendia, Makati and in España, Manila, and offered basic computer programming courses. With a unique and superior product on their hands, it was not difficult to expand the franchise
            through the founders’ business contacts. A year after the first two schools opened, the franchise grew to include STI Binondo, Cubao, and Taft. </p>
        <p>A unique value proposition spelled the difference for the STI brand then: “First We’ll Teach You, Then We’ll Hire You.” Through its unique Guaranteed Hire Program (GHP), all qualified graduates were offered jobs by one of the founders’ companies,
            or through their contacts in the industry. </p>
        <p>The schools’ 1st batch of graduates, all 11 of them, were hired by Systems Resources &lt;br&gt;Incorporated. And through GHP, more qualified STI graduates found themselves working in their field of interest straight out of school. </p>
        <p>No one among the four founders imagined that the Systems Technology Institute would become a college, or would grow to have over 100 schools across the country. But it did, all because of its unique value proposition, the synergy between the founders
            and their personnel, and the management’s faithfulness to quality. </p>
        <p>A long way since its birth, STI’s thrust has permeated right into the core of the globally competitive market — it has transcended beyond ICT and beyond education, addressing the need for job-ready graduates. </p>
        <div></div>
        <div class="thumbnail"><img src="assets/img/history.jpg"></div>
    </div>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
</body>

</html>