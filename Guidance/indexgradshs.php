<?php
session_start();

	if(isset($_SESSION["username"])==false)// ginagamit yan para di ka makapunta sa 2nd page, need to log in first para makapunta sa next page.
	{
		header("location:index.php");
	}
$connect = mysqli_connect("localhost", "root", "", "guidance_db");
 
$query = "SELECT StudentNumber,Lastname,Course,Year FROM tbl_seniorhigh";
$result = mysqli_query($connect, $query);
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Grades | Tertiary Students</title>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
		<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
		<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>		
		<link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
   	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Cookie">
    <link rel="stylesheet" href="assets/fonts/font-awesome.min.css">
    <link rel="stylesheet" href="assets/fonts/material-icons.css">
    <link rel="stylesheet" href="assets/css/styles.css">

	</head>
	<body>
		<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header"><a href="indexgradshs.php" class="navbar-brand navbar-link"><strong>STI College Meycauayan</strong>     Senior High Students</a>
            <button data-toggle="collapse" data-target="#navcol-1" class="navbar-toggle collapsed"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
        </div>

        <div class="collapse navbar-collapse" id="navcol-1">
            
                
                <ul class="nav navbar-nav navbar-right">
                <li role="presentation"><a href="indexAnnounce.php">Announcements </a></li>
                <li class="dropdown"><a data-toggle="dropdown" aria-expanded="false" href="#" class="dropdown-toggle">Upload CSV File<span class="caret"></span></a>
                    <ul role="menu" class="dropdown-menu">
                        <li role="presentation"><a href="tertiarycsv.php">Tertiary Students</a></li>
                        <li role="presentation"><a href="seniorcsv.php">Senior High Students</a></li>
                        
                    </ul>
                </li>
                
                <li class="dropdown"><a data-toggle="dropdown" aria-expanded="false" href="#" class="dropdown-toggle">Grades <span class="caret"></span></a>
                    <ul role="menu" class="dropdown-menu">
                        <li role="presentation"><a href="indexgradter.php">Tertiary Students</a></li>
                        <li role="presentation"><a href="indexgradshs.php">Senior High Students</a></li>
                    </ul>

                    <li class="dropdown"><a data-toggle="dropdown" aria-expanded="false" href="#" class="dropdown-toggle">Offenses <span class="caret"></span></a>
                <ul role="menu" class="dropdown-menu">
                    <li role="presentation"><a href="indexteroff.php">Tertiary Students</a></li>
                    <li role="presentation"><a href="indexshsoff.php">Senior High Students</a></li>
                </ul>

                <li class="dropdown"><a data-toggle="dropdown" aria-expanded="false" href="#" class="dropdown-toggle">About STI<span class="caret"></span></a>
                    <ul role="menu" class="dropdown-menu">
                        <li role="presentation"><a href="history.php">History</a></li>
                        <li role="presentation"><a href="vision.php">Vision, Mission and Hymn</a></li>
                        
                    </ul>
                </li>
                
                <li class="dropdown"><a data-toggle="dropdown" aria-expanded="false" href="#" class="dropdown-toggle">
                	<?php echo $_SESSION['username']; ?><span class="caret"></span></a>
                    <ul role="menu" class="dropdown-menu">
                        <li role="presentation"><a href="updateadmin.php">Edit Profile</a></li>
                        <li role="presentation"><a href="addusers.php">Add Users</a></li>
                        <li role="presentation"><a href="addoffenses.php">Add Offenses</a></li>
                        <li role="presentation"><a href="index.php?action=logout">Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav><br><br><br>
	<br />
	<div class="col-md-12"> <!-- Malaking box na kulay blue Students account simula  -->
    <div class="panel panel-primary">
      <div class="panel-heading" style="color:yellow;">Lists of Students</div>
      <div class="panel-body">
        <div class="col-md-12"> <!-- hanggang dito  -->

	
			
			<div class="table-responsive">
				<table class="table table-bordered table-striped">
					<tr>
			<div class="table-responsive">
				<br />
				<div align="right">
					<a href="welcome.php?action=back" class="btn btn-info">Back</a> 
				</div>
				<table id="user_data" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>Student Number</th>
							<th>Lastname</th>
							<th>Course</th>
							<th>Year</th>
							<th>Edit</th>
							<th>Delete</th>
						</tr>
					</thead>
				</table>
				
			</div>
		</div>
		</div> <!-- closing tag ng malaking box na student accounts simula  -->
</div>	
</div>
</div>
</div> <!-- hanggang dito  -->

	</body>
</html>

<div id="userModal" class="modal fade">
	<div class="modal-dialog" style="width: 800px;">
		<form method="post" id="user_form" enctype="multipart/form-data">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Add Student</h4>
				</div>
				<div class="modal-body">
					<div class="table-responsive">
					
					<!--PRELIM -->
					<div class="table-responsive">
						<table class="table table-bordered">
							<tbody>
									<CENTER><h3><label>Prelim</label></h3></CENTER> 
								<tr>
									<td width="50%"><label>Subject 1</label><input type="text" name="Subject1" id="Subject1" class="form-control" placeholder="Input Subject" /></td>
									<td width="50%"><label>Grade    </label><select name="Grade1" id="Grade1" class="form-control" />
																	  <option value="" disabled selected>Select Grades</option> 
																	  <option value="No Grades yet.">No Grades yet.</option>
																	  <option value="1.00">1.00 (98-100%)</option>
																	  <option value="1.25">1.25 (95-97.99%)</option>
																	  <option value="1.50">1.50 (92-94.99%)</option>
																	  <option value="1.75">1.75 (89-91.99%)</option>
																	  <option value="2.00">2.00 (86-88.99%)</option>
																	  <option value="2.25">2.25 (83-85.99%)</option>
																	  <option value="2.50">2.50 (80-82.99%)</option>
																	  <option value="2.75">2.75 (77-79.99%)</option>
																	  <option value="3.00">3.00 (75-76.99%)</option>
																	  <option value="5.00">5.00 (0-74.99%)</option>
																	  <option value="Officially Drop">Officially Drop</option>
																	  <option value="Passed">Passed</option>
																	  <option value="Failed">Failed</option>
																</select></td>
								</tr>
								<tr>
									<td width="50%"><label>Subject 2</label><input type="text" name="Subject2" id="Subject2" class="form-control" placeholder="Input Subject"/></td>
									<td width="50%"><label>Grade    </label></label><select name="Grade2" id="Grade2" class="form-control" /> 
																	  <option value="" disabled selected>Select Grades</option>
																	  <option value="No Grades yet.">No Grades yet.</option>
																	  <option value="1.00">1.00 (98-100%)</option>
																	  <option value="1.25">1.25 (95-97.99%)</option>
																	  <option value="1.50">1.50 (92-94.99%)</option>
																	  <option value="1.75">1.75 (89-91.99%)</option>
																	  <option value="2.00">2.00 (86-88.99%)</option>
																	  <option value="2.25">2.25 (83-85.99%)</option>
																	  <option value="2.50">2.50 (80-82.99%)</option>
																	  <option value="2.75">2.75 (77-79.99%)</option>
																	  <option value="3.00">3.00 (75-76.99%)</option>
																	  <option value="5.00">5.00 (0-74.99%)</option>
																	  <option value="Officially Drop">Officially Drop</option>
																	  <option value="Passed">Passed</option>
																	  <option value="Failed">Failed</option>
																</select></td>  
								</tr>  

								<tr>
									<td width="50%"><label>Subject 3</label><input type="text" name="Subject3" id="Subject3" class="form-control" placeholder="Input Subject"/></td>
									<td width="50%"><label>Grade    </label></label><select name="Grade3" id="Grade3" class="form-control" /> 
																	  <option value="" disabled selected>Select Grades</option>
																	  <option value="No Grades yet.">No Grades yet.</option>
																	  <option value="1.00">1.00 (98-100%)</option>
																	  <option value="1.25">1.25 (95-97.99%)</option>
																	  <option value="1.50">1.50 (92-94.99%)</option>
																	  <option value="1.75">1.75 (89-91.99%)</option>
																	  <option value="2.00">2.00 (86-88.99%)</option>
																	  <option value="2.25">2.25 (83-85.99%)</option>
																	  <option value="2.50">2.50 (80-82.99%)</option>
																	  <option value="2.75">2.75 (77-79.99%)</option>
																	  <option value="3.00">3.00 (75-76.99%)</option>
																	  <option value="5.00">5.00 (0-74.99%)</option>
																	  <option value="Officially Drop">Officially Drop</option>
																	  <option value="Passed">Passed</option>
																	  <option value="Failed">Failed</option>
																</select></td>  
								</tr>  
								<tr>
									<td width="50%"><label>Subject 4</label><input type="text" name="Subject4" id="Subject4" class="form-control" placeholder="Input Subject"/></td>
									<td width="50%"><label>Grade    </label></label><select name="Grade4" id="Grade4" class="form-control" /> 
																	  <option value="" disabled selected>Select Grades</option>
																	  <option value="No Grades yet.">No Grades yet.</option>
																	  <option value="1.00">1.00 (98-100%)</option>
																	  <option value="1.25">1.25 (95-97.99%)</option>
																	  <option value="1.50">1.50 (92-94.99%)</option>
																	  <option value="1.75">1.75 (89-91.99%)</option>
																	  <option value="2.00">2.00 (86-88.99%)</option>
																	  <option value="2.25">2.25 (83-85.99%)</option>
																	  <option value="2.50">2.50 (80-82.99%)</option>
																	  <option value="2.75">2.75 (77-79.99%)</option>
																	  <option value="3.00">3.00 (75-76.99%)</option>
																	  <option value="5.00">5.00 (0-74.99%)</option>
																	  <option value="Officially Drop">Officially Drop</option>
																	  <option value="Passed">Passed</option>
																	  <option value="Failed">Failed</option>
																</select></td>  
								</tr> 
								<tr>
									<td width="50%"><label>Subject 5</label><input type="text" name="Subject5" id="Subject5" class="form-control" placeholder="Input Subject"/></td>
									<td width="50%"><label>Grade    </label></label><select name="Grade5" id="Grade5" class="form-control" /> 
																	  <option value="" disabled selected>Select Grades</option>
																	  <option value="No Grades yet.">No Grades yet.</option>
																	  <option value="1.00">1.00 (98-100%)</option>
																	  <option value="1.25">1.25 (95-97.99%)</option>
																	  <option value="1.50">1.50 (92-94.99%)</option>
																	  <option value="1.75">1.75 (89-91.99%)</option>
																	  <option value="2.00">2.00 (86-88.99%)</option>
																	  <option value="2.25">2.25 (83-85.99%)</option>
																	  <option value="2.50">2.50 (80-82.99%)</option>
																	  <option value="2.75">2.75 (77-79.99%)</option>
																	  <option value="3.00">3.00 (75-76.99%)</option>
																	  <option value="5.00">5.00 (0-74.99%)</option>
																	  <option value="Officially Drop">Officially Drop</option>
																	  <option value="Passed">Passed</option>
																	  <option value="Failed">Failed</option>
																</select></td>  
								</tr> 
								<tr>
									<td width="50%"><label>Subject 6</label><input type="text" name="Subject6" id="Subject6" class="form-control" placeholder="Input Subject"/></td>
									<td width="50%"><label>Grade    </label></label><select name="Grade6" id="Grade6" class="form-control" /> 
																	  <option value="" disabled selected>Select Grades</option>
																	  <option value="No Grades yet.">No Grades yet.</option>
																	  <option value="1.00">1.00 (98-100%)</option>
																	  <option value="1.25">1.25 (95-97.99%)</option>
																	  <option value="1.50">1.50 (92-94.99%)</option>
																	  <option value="1.75">1.75 (89-91.99%)</option>
																	  <option value="2.00">2.00 (86-88.99%)</option>
																	  <option value="2.25">2.25 (83-85.99%)</option>
																	  <option value="2.50">2.50 (80-82.99%)</option>
																	  <option value="2.75">2.75 (77-79.99%)</option>
																	  <option value="3.00">3.00 (75-76.99%)</option>
																	  <option value="5.00">5.00 (0-74.99%)</option>
																	  <option value="Officially Drop">Officially Drop</option>
																	  <option value="Passed">Passed</option>
																	  <option value="Failed">Failed</option>
																</select></td>  
								</tr> 
								<tr>
									<td width="50%"><label>Subject 7</label><input type="text" name="Subject7" id="Subject7" class="form-control" placeholder="Input Subject"/></td>
									<td width="50%"><label>Grade    </label></label><select name="Grade7" id="Grade7" class="form-control" /> 
																	  <option value="" disabled selected>Select Grades</option>
																	  <option value="No Grades yet.">No Grades yet.</option>
																	  <option value="1.00">1.00 (98-100%)</option>
																	  <option value="1.25">1.25 (95-97.99%)</option>
																	  <option value="1.50">1.50 (92-94.99%)</option>
																	  <option value="1.75">1.75 (89-91.99%)</option>
																	  <option value="2.00">2.00 (86-88.99%)</option>
																	  <option value="2.25">2.25 (83-85.99%)</option>
																	  <option value="2.50">2.50 (80-82.99%)</option>
																	  <option value="2.75">2.75 (77-79.99%)</option>
																	  <option value="3.00">3.00 (75-76.99%)</option>
																	  <option value="5.00">5.00 (0-74.99%)</option>
																	  <option value="Officially Drop">Officially Drop</option>
																	  <option value="Passed">Passed</option>
																	  <option value="Failed">Failed</option>
																</select></td>  
								</tr> 
								<tr>
									<td width="50%"><label>Subject 8</label><input type="text" name="Subject8" id="Subject8" class="form-control" placeholder="Input Subject"/></td>
									<td width="50%"><label>Grade    </label></label><select name="Grade8" id="Grade8" class="form-control" /> 
																	  <option value="" disabled selected>Select Grades</option>
																	  <option value="No Grades yet.">No Grades yet.</option>
																	  <option value="1.00">1.00 (98-100%)</option>
																	  <option value="1.25">1.25 (95-97.99%)</option>
																	  <option value="1.50">1.50 (92-94.99%)</option>
																	  <option value="1.75">1.75 (89-91.99%)</option>
																	  <option value="2.00">2.00 (86-88.99%)</option>
																	  <option value="2.25">2.25 (83-85.99%)</option>
																	  <option value="2.50">2.50 (80-82.99%)</option>
																	  <option value="2.75">2.75 (77-79.99%)</option>
																	  <option value="3.00">3.00 (75-76.99%)</option>
																	  <option value="5.00">5.00 (0-74.99%)</option>
																	  <option value="Officially Drop">Officially Drop</option>
																	  <option value="Passed">Passed</option>
																	  <option value="Failed">Failed</option>
																</select></td>  
								</tr> 
							</tbody>
						</table>
					</div>

<!--MIDTERM -->
					<div class="table-responsive">     
						<table class="table table-bordered">
							<tbody>
									<CENTER><h3><label>Midterm</label></h3></CENTER>
								<tr>
									<td width="50%"><label>Subject 1</label><input type="text" name="Subject9" id="Subject9" class="form-control" placeholder="Input Subject" /></td>
									<td width="50%"><label>Grade    </label><select name="Grade9" id="Grade9" class="form-control" />
																	  <option value="" disabled selected>Select Grades</option> 
																	  <option value="No Grades yet.">No Grades yet.</option>
																	  <option value="1.00">1.00 (98-100%)</option>
																	  <option value="1.25">1.25 (95-97.99%)</option>
																	  <option value="1.50">1.50 (92-94.99%)</option>
																	  <option value="1.75">1.75 (89-91.99%)</option>
																	  <option value="2.00">2.00 (86-88.99%)</option>
																	  <option value="2.25">2.25 (83-85.99%)</option>
																	  <option value="2.50">2.50 (80-82.99%)</option>
																	  <option value="2.75">2.75 (77-79.99%)</option>
																	  <option value="3.00">3.00 (75-76.99%)</option>
																	  <option value="5.00">5.00 (0-74.99%)</option>
																	  <option value="Officially Drop">Officially Drop</option>
																	  <option value="Passed">Passed</option>
																	  <option value="Failed">Failed</option>
																</select></td>
								</tr>
								<tr>
									<td width="50%"><label>Subject 2</label><input type="text" name="Subject10" id="Subject10" class="form-control" placeholder="Input Subject"/></td>
									<td width="50%"><label>Grade    </label></label><select name="Grade10" id="Grade10" class="form-control" /> 
																	  <option value="" disabled selected>Select Grades</option>
																	  <option value="No Grades yet.">No Grades yet.</option>
																	  <option value="1.00">1.00 (98-100%)</option>
																	  <option value="1.25">1.25 (95-97.99%)</option>
																	  <option value="1.50">1.50 (92-94.99%)</option>
																	  <option value="1.75">1.75 (89-91.99%)</option>
																	  <option value="2.00">2.00 (86-88.99%)</option>
																	  <option value="2.25">2.25 (83-85.99%)</option>
																	  <option value="2.50">2.50 (80-82.99%)</option>
																	  <option value="2.75">2.75 (77-79.99%)</option>
																	  <option value="3.00">3.00 (75-76.99%)</option>
																	  <option value="5.00">5.00 (0-74.99%)</option>
																	  <option value="Officially Drop">Officially Drop</option>
																	  <option value="Passed">Passed</option>
																	  <option value="Failed">Failed</option>
																</select></td>  
								</tr>  

								<tr>
									<td width="50%"><label>Subject 3</label><input type="text" name="Subject11" id="Subject11" class="form-control" placeholder="Input Subject"/></td>
									<td width="50%"><label>Grade    </label></label><select name="Grade11" id="Grade11" class="form-control" /> 
																	  <option value="" disabled selected>Select Grades</option>
																	  <option value="No Grades yet.">No Grades yet.</option>
																	  <option value="1.00">1.00 (98-100%)</option>
																	  <option value="1.25">1.25 (95-97.99%)</option>
																	  <option value="1.50">1.50 (92-94.99%)</option>
																	  <option value="1.75">1.75 (89-91.99%)</option>
																	  <option value="2.00">2.00 (86-88.99%)</option>
																	  <option value="2.25">2.25 (83-85.99%)</option>
																	  <option value="2.50">2.50 (80-82.99%)</option>
																	  <option value="2.75">2.75 (77-79.99%)</option>
																	  <option value="3.00">3.00 (75-76.99%)</option>
																	  <option value="5.00">5.00 (0-74.99%)</option>
																	  <option value="Officially Drop">Officially Drop</option>
																	  <option value="Passed">Passed</option>
																	  <option value="Failed">Failed</option>
																</select></td>  
								</tr>  
								<tr>
									<td width="50%"><label>Subject 4</label><input type="text" name="Subject12" id="Subject12" class="form-control" placeholder="Input Subject"/></td>
									<td width="50%"><label>Grade    </label></label><select name="Grade12" id="Grade12" class="form-control" /> 
																	  <option value="" disabled selected>Select Grades</option>
																	  <option value="No Grades yet.">No Grades yet.</option>
																	  <option value="1.00">1.00 (98-100%)</option>
																	  <option value="1.25">1.25 (95-97.99%)</option>
																	  <option value="1.50">1.50 (92-94.99%)</option>
																	  <option value="1.75">1.75 (89-91.99%)</option>
																	  <option value="2.00">2.00 (86-88.99%)</option>
																	  <option value="2.25">2.25 (83-85.99%)</option>
																	  <option value="2.50">2.50 (80-82.99%)</option>
																	  <option value="2.75">2.75 (77-79.99%)</option>
																	  <option value="3.00">3.00 (75-76.99%)</option>
																	  <option value="5.00">5.00 (0-74.99%)</option>
																	  <option value="Officially Drop">Officially Drop</option>
																	  <option value="Passed">Passed</option>
																	  <option value="Failed">Failed</option>
																</select></td>  
								</tr> 
								<tr>
									<td width="50%"><label>Subject 5</label><input type="text" name="Subject13" id="Subject13" class="form-control" placeholder="Input Subject"/></td>
									<td width="50%"><label>Grade    </label></label><select name="Grade13" id="Grade13" class="form-control" /> 
																	  <option value="" disabled selected>Select Grades</option>
																	  <option value="No Grades yet.">No Grades yet.</option>
																	  <option value="1.00">1.00 (98-100%)</option>
																	  <option value="1.25">1.25 (95-97.99%)</option>
																	  <option value="1.50">1.50 (92-94.99%)</option>
																	  <option value="1.75">1.75 (89-91.99%)</option>
																	  <option value="2.00">2.00 (86-88.99%)</option>
																	  <option value="2.25">2.25 (83-85.99%)</option>
																	  <option value="2.50">2.50 (80-82.99%)</option>
																	  <option value="2.75">2.75 (77-79.99%)</option>
																	  <option value="3.00">3.00 (75-76.99%)</option>
																	  <option value="5.00">5.00 (0-74.99%)</option>
																	  <option value="Officially Drop">Officially Drop</option>
																	  <option value="Passed">Passed</option>
																	  <option value="Failed">Failed</option>
																</select></td>  
								</tr> 
								<tr>
									<td width="50%"><label>Subject 6</label><input type="text" name="Subject14" id="Subject14" class="form-control" placeholder="Input Subject"/></td>
									<td width="50%"><label>Grade    </label></label><select name="Grade14" id="Grade14" class="form-control" /> 
																	  <option value="" disabled selected>Select Grades</option>
																	  <option value="No Grades yet.">No Grades yet.</option>
																	  <option value="1.00">1.00 (98-100%)</option>
																	  <option value="1.25">1.25 (95-97.99%)</option>
																	  <option value="1.50">1.50 (92-94.99%)</option>
																	  <option value="1.75">1.75 (89-91.99%)</option>
																	  <option value="2.00">2.00 (86-88.99%)</option>
																	  <option value="2.25">2.25 (83-85.99%)</option>
																	  <option value="2.50">2.50 (80-82.99%)</option>
																	  <option value="2.75">2.75 (77-79.99%)</option>
																	  <option value="3.00">3.00 (75-76.99%)</option>
																	  <option value="5.00">5.00 (0-74.99%)</option>
																	  <option value="Officially Drop">Officially Drop</option>
																	  <option value="Passed">Passed</option>
																	  <option value="Failed">Failed</option>
																</select></td>  
								</tr> 
								<tr>
									<td width="50%"><label>Subject 7</label><input type="text" name="Subject15" id="Subject15" class="form-control" placeholder="Input Subject"/></td>
									<td width="50%"><label>Grade    </label></label><select name="Grade15" id="Grade15" class="form-control" /> 
																	  <option value="" disabled selected>Select Grades</option>
																	  <option value="No Grades yet.">No Grades yet.</option>
																	  <option value="1.00">1.00 (98-100%)</option>
																	  <option value="1.25">1.25 (95-97.99%)</option>
																	  <option value="1.50">1.50 (92-94.99%)</option>
																	  <option value="1.75">1.75 (89-91.99%)</option>
																	  <option value="2.00">2.00 (86-88.99%)</option>
																	  <option value="2.25">2.25 (83-85.99%)</option>
																	  <option value="2.50">2.50 (80-82.99%)</option>
																	  <option value="2.75">2.75 (77-79.99%)</option>
																	  <option value="3.00">3.00 (75-76.99%)</option>
																	  <option value="5.00">5.00 (0-74.99%)</option>
																	  <option value="Officially Drop">Officially Drop</option>
																	  <option value="Passed">Passed</option>
																	  <option value="Failed">Failed</option>
																</select></td>  
								</tr> 
								<tr>
									<td width="50%"><label>Subject 8</label><input type="text" name="Subject16" id="Subject16" class="form-control" placeholder="Input Subject"/></td>
									<td width="50%"><label>Grade    </label></label><select name="Grade16" id="Grade16" class="form-control" /> 
																	  <option value="" disabled selected>Select Grades</option>
																	  <option value="No Grades yet.">No Grades yet.</option>
																	  <option value="1.00">1.00 (98-100%)</option>
																	  <option value="1.25">1.25 (95-97.99%)</option>
																	  <option value="1.50">1.50 (92-94.99%)</option>
																	  <option value="1.75">1.75 (89-91.99%)</option>
																	  <option value="2.00">2.00 (86-88.99%)</option>
																	  <option value="2.25">2.25 (83-85.99%)</option>
																	  <option value="2.50">2.50 (80-82.99%)</option>
																	  <option value="2.75">2.75 (77-79.99%)</option>
																	  <option value="3.00">3.00 (75-76.99%)</option>
																	  <option value="5.00">5.00 (0-74.99%)</option>
																	  <option value="Officially Drop">Officially Drop</option>
																	  <option value="Passed">Passed</option>
																	  <option value="Failed">Failed</option>
																</select></td>  
								</tr>
							</tbody>
						</table>
					</div>

<!--PRE FINAL -->
					<div class="table-responsive">     
						<table class="table table-bordered">
							<tbody>
									<CENTER><h3><label>Pre-Final</label></h3></CENTER>
								<tr>
									<td width="50%"><label>Subject 1</label><input type="text" name="Subject17" id="Subject17" class="form-control" placeholder="Input Subject" /></td>
									<td width="50%"><label>Grade    </label><select name="Grade17" id="Grade17" class="form-control" />
																	  <option value="" disabled selected>Select Grades</option> 
																	  <option value="No Grades yet.">No Grades yet.</option>
																	  <option value="1.00">1.00 (98-100%)</option>
																	  <option value="1.25">1.25 (95-97.99%)</option>
																	  <option value="1.50">1.50 (92-94.99%)</option>
																	  <option value="1.75">1.75 (89-91.99%)</option>
																	  <option value="2.00">2.00 (86-88.99%)</option>
																	  <option value="2.25">2.25 (83-85.99%)</option>
																	  <option value="2.50">2.50 (80-82.99%)</option>
																	  <option value="2.75">2.75 (77-79.99%)</option>
																	  <option value="3.00">3.00 (75-76.99%)</option>
																	  <option value="5.00">5.00 (0-74.99%)</option>
																	  <option value="Officially Drop">Officially Drop</option>
																	  <option value="Passed">Passed</option>
																	  <option value="Failed">Failed</option>
																</select></td>
								</tr>
								<tr>
									<td width="50%"><label>Subject 2</label><input type="text" name="Subject18" id="Subject18" class="form-control" placeholder="Input Subject"/></td>
									<td width="50%"><label>Grade    </label></label><select name="Grade18" id="Grade18" class="form-control" /> 
																	  <option value="" disabled selected>Select Grades</option>
																	  <option value="No Grades yet.">No Grades yet.</option>
																	  <option value="1.00">1.00 (98-100%)</option>
																	  <option value="1.25">1.25 (95-97.99%)</option>
																	  <option value="1.50">1.50 (92-94.99%)</option>
																	  <option value="1.75">1.75 (89-91.99%)</option>
																	  <option value="2.00">2.00 (86-88.99%)</option>
																	  <option value="2.25">2.25 (83-85.99%)</option>
																	  <option value="2.50">2.50 (80-82.99%)</option>
																	  <option value="2.75">2.75 (77-79.99%)</option>
																	  <option value="3.00">3.00 (75-76.99%)</option>
																	  <option value="5.00">5.00 (0-74.99%)</option>
																	  <option value="Officially Drop">Officially Drop</option>
																	  <option value="Passed">Passed</option>
																	  <option value="Failed">Failed</option>
																</select></td>  
								</tr>  

								<tr>
									<td width="50%"><label>Subject 3</label><input type="text" name="Subject19" id="Subject19" class="form-control" placeholder="Input Subject"/></td>
									<td width="50%"><label>Grade    </label></label><select name="Grade19" id="Grade19" class="form-control" /> 
																	  <option value="" disabled selected>Select Grades</option>
																	  <option value="No Grades yet.">No Grades yet.</option>
																	  <option value="1.00">1.00 (98-100%)</option>
																	  <option value="1.25">1.25 (95-97.99%)</option>
																	  <option value="1.50">1.50 (92-94.99%)</option>
																	  <option value="1.75">1.75 (89-91.99%)</option>
																	  <option value="2.00">2.00 (86-88.99%)</option>
																	  <option value="2.25">2.25 (83-85.99%)</option>
																	  <option value="2.50">2.50 (80-82.99%)</option>
																	  <option value="2.75">2.75 (77-79.99%)</option>
																	  <option value="3.00">3.00 (75-76.99%)</option>
																	  <option value="5.00">5.00 (0-74.99%)</option>
																	  <option value="Officially Drop">Officially Drop</option>
																	  <option value="Passed">Passed</option>
																	  <option value="Failed">Failed</option>
																</select></td>  
								</tr>  
								<tr>
									<td width="50%"><label>Subject 4</label><input type="text" name="Subject20" id="Subject20" class="form-control" placeholder="Input Subject"/></td>
									<td width="50%"><label>Grade    </label></label><select name="Grade20" id="Grade20" class="form-control" /> 
																	  <option value="" disabled selected>Select Grades</option>
																	  <option value="No Grades yet.">No Grades yet.</option>
																	  <option value="1.00">1.00 (98-100%)</option>
																	  <option value="1.25">1.25 (95-97.99%)</option>
																	  <option value="1.50">1.50 (92-94.99%)</option>
																	  <option value="1.75">1.75 (89-91.99%)</option>
																	  <option value="2.00">2.00 (86-88.99%)</option>
																	  <option value="2.25">2.25 (83-85.99%)</option>
																	  <option value="2.50">2.50 (80-82.99%)</option>
																	  <option value="2.75">2.75 (77-79.99%)</option>
																	  <option value="3.00">3.00 (75-76.99%)</option>
																	  <option value="5.00">5.00 (0-74.99%)</option>
																	  <option value="Officially Drop">Officially Drop</option>
																	  <option value="Passed">Passed</option>
																	  <option value="Failed">Failed</option>
																</select></td>  
								</tr> 
								<tr>
									<td width="50%"><label>Subject 5</label><input type="text" name="Subject21" id="Subject21" class="form-control" placeholder="Input Subject"/></td>
									<td width="50%"><label>Grade    </label></label><select name="Grade21" id="Grade21" class="form-control" /> 
																	  <option value="" disabled selected>Select Grades</option>
																	  <option value="No Grades yet.">No Grades yet.</option>
																	  <option value="1.00">1.00 (98-100%)</option>
																	  <option value="1.25">1.25 (95-97.99%)</option>
																	  <option value="1.50">1.50 (92-94.99%)</option>
																	  <option value="1.75">1.75 (89-91.99%)</option>
																	  <option value="2.00">2.00 (86-88.99%)</option>
																	  <option value="2.25">2.25 (83-85.99%)</option>
																	  <option value="2.50">2.50 (80-82.99%)</option>
																	  <option value="2.75">2.75 (77-79.99%)</option>
																	  <option value="3.00">3.00 (75-76.99%)</option>
																	  <option value="5.00">5.00 (0-74.99%)</option>
																	  <option value="Officially Drop">Officially Drop</option>
																	  <option value="Passed">Passed</option>
																	  <option value="Failed">Failed</option>
																</select></td>  
								</tr> 
								<tr>
									<td width="50%"><label>Subject 6</label><input type="text" name="Subject22" id="Subject22" class="form-control" placeholder="Input Subject"/></td>
									<td width="50%"><label>Grade    </label></label><select name="Grade22" id="Grade22" class="form-control" /> 
																	  <option value="" disabled selected>Select Grades</option>
																	  <option value="No Grades yet.">No Grades yet.</option>
																	  <option value="1.00">1.00 (98-100%)</option>
																	  <option value="1.25">1.25 (95-97.99%)</option>
																	  <option value="1.50">1.50 (92-94.99%)</option>
																	  <option value="1.75">1.75 (89-91.99%)</option>
																	  <option value="2.00">2.00 (86-88.99%)</option>
																	  <option value="2.25">2.25 (83-85.99%)</option>
																	  <option value="2.50">2.50 (80-82.99%)</option>
																	  <option value="2.75">2.75 (77-79.99%)</option>
																	  <option value="3.00">3.00 (75-76.99%)</option>
																	  <option value="5.00">5.00 (0-74.99%)</option>
																	  <option value="Officially Drop">Officially Drop</option>
																	  <option value="Passed">Passed</option>
																	  <option value="Failed">Failed</option>
																</select></td>  
								</tr> 
								<tr>
									<td width="50%"><label>Subject 7</label><input type="text" name="Subject23" id="Subject23" class="form-control" placeholder="Input Subject"/></td>
									<td width="50%"><label>Grade    </label></label><select name="Grade23" id="Grade23" class="form-control" /> 
																	  <option value="" disabled selected>Select Grades</option>
																	  <option value="No Grades yet.">No Grades yet.</option>
																	  <option value="1.00">1.00 (98-100%)</option>
																	  <option value="1.25">1.25 (95-97.99%)</option>
																	  <option value="1.50">1.50 (92-94.99%)</option>
																	  <option value="1.75">1.75 (89-91.99%)</option>
																	  <option value="2.00">2.00 (86-88.99%)</option>
																	  <option value="2.25">2.25 (83-85.99%)</option>
																	  <option value="2.50">2.50 (80-82.99%)</option>
																	  <option value="2.75">2.75 (77-79.99%)</option>
																	  <option value="3.00">3.00 (75-76.99%)</option>
																	  <option value="5.00">5.00 (0-74.99%)</option>
																	  <option value="Officially Drop">Officially Drop</option>
																	  <option value="Passed">Passed</option>
																	  <option value="Failed">Failed</option>
																</select></td>  
								</tr> 
								<tr>
									<td width="50%"><label>Subject 8</label><input type="text" name="Subject24" id="Subject24" class="form-control" placeholder="Input Subject"/></td>
									<td width="50%"><label>Grade    </label></label><select name="Grade24" id="Grade24" class="form-control" /> 
																	  <option value="" disabled selected>Select Grades</option>
																	  <option value="No Grades yet.">No Grades yet.</option>
																	  <option value="1.00">1.00 (98-100%)</option>
																	  <option value="1.25">1.25 (95-97.99%)</option>
																	  <option value="1.50">1.50 (92-94.99%)</option>
																	  <option value="1.75">1.75 (89-91.99%)</option>
																	  <option value="2.00">2.00 (86-88.99%)</option>
																	  <option value="2.25">2.25 (83-85.99%)</option>
																	  <option value="2.50">2.50 (80-82.99%)</option>
																	  <option value="2.75">2.75 (77-79.99%)</option>
																	  <option value="3.00">3.00 (75-76.99%)</option>
																	  <option value="5.00">5.00 (0-74.99%)</option>
																	  <option value="Officially Drop">Officially Drop</option>
																	  <option value="Passed">Passed</option>
																	  <option value="Failed">Failed</option>
																</select></td>  
								</tr>
							</tbody>
						</table>
					</div>

<!--FINAL -->
					<div class="table-responsive">     
						<table class="table table-bordered">
							<tbody>
								<center>	<h3><label>Final</label></h3></center>
								<tr>
									<td width="50%"><label>Subject 1</label><input type="text" name="Subject25" id="Subject25" class="form-control" placeholder="Input Subject" /></td>
									<td width="50%"><label>Grade    </label><select name="Grade25" id="Grade25" class="form-control" />
																	  <option value="" disabled selected>Select Grades</option> 
																	  <option value="No Grades yet.">No Grades yet.</option>
																	  <option value="1.00">1.00 (98-100%)</option>
																	  <option value="1.25">1.25 (95-97.99%)</option>
																	  <option value="1.50">1.50 (92-94.99%)</option>
																	  <option value="1.75">1.75 (89-91.99%)</option>
																	  <option value="2.00">2.00 (86-88.99%)</option>
																	  <option value="2.25">2.25 (83-85.99%)</option>
																	  <option value="2.50">2.50 (80-82.99%)</option>
																	  <option value="2.75">2.75 (77-79.99%)</option>
																	  <option value="3.00">3.00 (75-76.99%)</option>
																	  <option value="5.00">5.00 (0-74.99%)</option>
																	  <option value="Officially Drop">Officially Drop</option>
																	  <option value="Passed">Passed</option>
																	  <option value="Failed">Failed</option>
																</select></td>
								</tr>
								<tr>
									<td width="50%"><label>Subject 2</label><input type="text" name="Subject26" id="Subject26" class="form-control" placeholder="Input Subject"/></td>
									<td width="50%"><label>Grade    </label></label><select name="Grade26" id="Grade26" class="form-control" /> 
																	  <option value="" disabled selected>Select Grades</option>
																	  <option value="No Grades yet.">No Grades yet.</option>
																	  <option value="1.00">1.00 (98-100%)</option>
																	  <option value="1.25">1.25 (95-97.99%)</option>
																	  <option value="1.50">1.50 (92-94.99%)</option>
																	  <option value="1.75">1.75 (89-91.99%)</option>
																	  <option value="2.00">2.00 (86-88.99%)</option>
																	  <option value="2.25">2.25 (83-85.99%)</option>
																	  <option value="2.50">2.50 (80-82.99%)</option>
																	  <option value="2.75">2.75 (77-79.99%)</option>
																	  <option value="3.00">3.00 (75-76.99%)</option>
																	  <option value="5.00">5.00 (0-74.99%)</option>
																	  <option value="Officially Drop">Officially Drop</option>
																	  <option value="Passed">Passed</option>
																	  <option value="Failed">Failed</option>
																</select></td>  
								</tr>  

								<tr>
									<td width="50%"><label>Subject 3</label><input type="text" name="Subject27" id="Subject27" class="form-control" placeholder="Input Subject"/></td>
									<td width="50%"><label>Grade    </label></label><select name="Grade27" id="Grade27" class="form-control" /> 
																	  <option value="" disabled selected>Select Grades</option>
																	  <option value="No Grades yet.">No Grades yet.</option>
																	  <option value="1.00">1.00 (98-100%)</option>
																	  <option value="1.25">1.25 (95-97.99%)</option>
																	  <option value="1.50">1.50 (92-94.99%)</option>
																	  <option value="1.75">1.75 (89-91.99%)</option>
																	  <option value="2.00">2.00 (86-88.99%)</option>
																	  <option value="2.25">2.25 (83-85.99%)</option>
																	  <option value="2.50">2.50 (80-82.99%)</option>
																	  <option value="2.75">2.75 (77-79.99%)</option>
																	  <option value="3.00">3.00 (75-76.99%)</option>
																	  <option value="5.00">5.00 (0-74.99%)</option>
																	  <option value="Officially Drop">Officially Drop</option>
																	  <option value="Passed">Passed</option>
																	  <option value="Failed">Failed</option>
																</select></td>  
								</tr>  
								<tr>
									<td width="50%"><label>Subject 4</label><input type="text" name="Subject28" id="Subject28" class="form-control" placeholder="Input Subject"/></td>
									<td width="50%"><label>Grade    </label></label><select name="Grade28" id="Grade28" class="form-control" /> 
																	  <option value="" disabled selected>Select Grades</option>
																	  <option value="No Grades yet.">No Grades yet.</option>
																	  <option value="1.00">1.00 (98-100%)</option>
																	  <option value="1.25">1.25 (95-97.99%)</option>
																	  <option value="1.50">1.50 (92-94.99%)</option>
																	  <option value="1.75">1.75 (89-91.99%)</option>
																	  <option value="2.00">2.00 (86-88.99%)</option>
																	  <option value="2.25">2.25 (83-85.99%)</option>
																	  <option value="2.50">2.50 (80-82.99%)</option>
																	  <option value="2.75">2.75 (77-79.99%)</option>
																	  <option value="3.00">3.00 (75-76.99%)</option>
																	  <option value="5.00">5.00 (0-74.99%)</option>
																	  <option value="Officially Drop">Officially Drop</option>
																	  <option value="Passed">Passed</option>
																	  <option value="Failed">Failed</option>
																</select></td>  
								</tr> 
								<tr>
									<td width="50%"><label>Subject 5</label><input type="text" name="Subject29" id="Subject29" class="form-control" placeholder="Input Subject"/></td>
									<td width="50%"><label>Grade    </label></label><select name="Grade29" id="Grade29" class="form-control" /> 
																	  <option value="" disabled selected>Select Grades</option>
																	  <option value="No Grades yet.">No Grades yet.</option>
																	  <option value="1.00">1.00 (98-100%)</option>
																	  <option value="1.25">1.25 (95-97.99%)</option>
																	  <option value="1.50">1.50 (92-94.99%)</option>
																	  <option value="1.75">1.75 (89-91.99%)</option>
																	  <option value="2.00">2.00 (86-88.99%)</option>
																	  <option value="2.25">2.25 (83-85.99%)</option>
																	  <option value="2.50">2.50 (80-82.99%)</option>
																	  <option value="2.75">2.75 (77-79.99%)</option>
																	  <option value="3.00">3.00 (75-76.99%)</option>
																	  <option value="5.00">5.00 (0-74.99%)</option>
																	  <option value="Officially Drop">Officially Drop</option>
																	  <option value="Passed">Passed</option>
																	  <option value="Failed">Failed</option>
																</select></td>  
								</tr> 
								<tr>
									<td width="50%"><label>Subject 6</label><input type="text" name="Subject30" id="Subject30" class="form-control" placeholder="Input Subject"/></td>
									<td width="50%"><label>Grade    </label></label><select name="Grade30" id="Grade30" class="form-control" /> 
																	  <option value="" disabled selected>Select Grades</option>
																	  <option value="No Grades yet.">No Grades yet.</option>
																	  <option value="1.00">1.00 (98-100%)</option>
																	  <option value="1.25">1.25 (95-97.99%)</option>
																	  <option value="1.50">1.50 (92-94.99%)</option>
																	  <option value="1.75">1.75 (89-91.99%)</option>
																	  <option value="2.00">2.00 (86-88.99%)</option>
																	  <option value="2.25">2.25 (83-85.99%)</option>
																	  <option value="2.50">2.50 (80-82.99%)</option>
																	  <option value="2.75">2.75 (77-79.99%)</option>
																	  <option value="3.00">3.00 (75-76.99%)</option>
																	  <option value="5.00">5.00 (0-74.99%)</option>
																	  <option value="Officially Drop">Officially Drop</option>
																	  <option value="Passed">Passed</option>
																	  <option value="Failed">Failed</option>
																</select></td>  
								</tr> 
								<tr>
									<td width="50%"><label>Subject 7</label><input type="text" name="Subject31" id="Subject31" class="form-control" placeholder="Input Subject"/></td>
									<td width="50%"><label>Grade    </label></label><select name="Grade31" id="Grade31" class="form-control" /> 
																	  <option value="" disabled selected>Select Grades</option>
																	  <option value="No Grades yet.">No Grades yet.</option>
																	  <option value="1.00">1.00 (98-100%)</option>
																	  <option value="1.25">1.25 (95-97.99%)</option>
																	  <option value="1.50">1.50 (92-94.99%)</option>
																	  <option value="1.75">1.75 (89-91.99%)</option>
																	  <option value="2.00">2.00 (86-88.99%)</option>
																	  <option value="2.25">2.25 (83-85.99%)</option>
																	  <option value="2.50">2.50 (80-82.99%)</option>
																	  <option value="2.75">2.75 (77-79.99%)</option>
																	  <option value="3.00">3.00 (75-76.99%)</option>
																	  <option value="5.00">5.00 (0-74.99%)</option>
																	  <option value="Officially Drop">Officially Drop</option>
																	  <option value="Passed">Passed</option>
																	  <option value="Failed">Failed</option>
																</select></td>  
								</tr> 
								<tr>
									<td width="50%"><label>Subject 8</label><input type="text" name="Subject32" id="Subject32" class="form-control" placeholder="Input Subject"/></td>
									<td width="50%"><label>Grade    </label></label><select name="Grade32" id="Grade32" class="form-control" /> 
																	  <option value="" disabled selected>Select Grades</option>
																	  <option value="No Grades yet.">No Grades yet.</option>
																	  <option value="1.00">1.00 (98-100%)</option>
																	  <option value="1.25">1.25 (95-97.99%)</option>
																	  <option value="1.50">1.50 (92-94.99%)</option>
																	  <option value="1.75">1.75 (89-91.99%)</option>
																	  <option value="2.00">2.00 (86-88.99%)</option>
																	  <option value="2.25">2.25 (83-85.99%)</option>
																	  <option value="2.50">2.50 (80-82.99%)</option>
																	  <option value="2.75">2.75 (77-79.99%)</option>
																	  <option value="3.00">3.00 (75-76.99%)</option>
																	  <option value="5.00">5.00 (0-74.99%)</option>
																	  <option value="Officially Drop">Officially Drop</option>
																	  <option value="Passed">Passed</option>
																	  <option value="Failed">Failed</option>
																</select></td>  
								</tr> 
							</tbody>
						</table>
					</div>
									


					</div>
					<br />
				</div>
				<div class="modal-footer">
					<input type="hidden" name="id" id="id" />
					<input type="hidden" name="operation" id="operation" />
					<input type="submit" name="action" id="action" class="btn btn-success" value="Add" />
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</form>
	</div>
</div>

<script type="text/javascript" language="javascript" >
$(document).ready(function(){
	$('#add_button').click(function(){
		$('#user_form')[0].reset();
		$('.modal-title').text("Add Student Offense");
		$('#action').val("Insert");
		$('#operation').val("Add");
	});
	
	var dataTable = $('#user_data').DataTable({
		"processing":true,
		"serverSide":true,
		"order":[],
		"ajax":{
			url:"fetchgradshs.php",
			type:"POST"
		},
		"columnDefs":[
			{
				"targets":[0],
				"orderable":false,
			},
		],

	});

	$(document).on('submit', '#user_form', function(event){
		event.preventDefault();
		var StudentNumber = $('#StudentNumber').val();
		var Lastname = $('#Lastname').val();
		var Course = $('#Course').val();
		var Year = $('#Year').val();
		if(StudentNumber != '' && Lastname != ''  && Course != '' && Year != '')
		{
			$.ajax({
				url:"insertgradshs.php",
				method:'POST',
				data:new FormData(this),
				contentType:false,
				processData:false,
				success:function(data)
				{
					alert(data);
					$('#user_form')[0].reset();
					$('#userModal').modal('hide');
					dataTable.ajax.reload();
				}
			});
		}
		else
		{
			alert("All Fields are Required");
		}
	});
	
	$(document).on('click', '.update', function(){
		var id = $(this).attr("id");
		$.ajax({
			url:"fetch_singlegradshs.php",
			method:"POST",
			data:{id:id},
			dataType:"json",
			success:function(data)
			{
				$('#userModal').modal('show');
				$('#StudentNumber').val(data.StudentNumber);
				$('#Lastname').val(data.Lastname);
				$('#Firstname').val(data.Firstname);
				$('#Course').val(data.Course);
				$('#Year').val(data.Year); 

				$('#Subject1').val(data.Subject1); 
				$('#Grade1').val(data.Grade1); 

				$('#Subject2').val(data.Subject2); 
				$('#Grade2').val(data.Grade2); 

				$('#Subject3').val(data.Subject3); 
				$('#Grade3').val(data.Grade3); 

				$('#Subject4').val(data.Subject4); 
				$('#Grade4').val(data.Grade4); 

				$('#Subject5').val(data.Subject5); 
				$('#Grade5').val(data.Grade5); 

				$('#Subject6').val(data.Subject6); 
				$('#Grade6').val(data.Grade6); 

				$('#Subject7').val(data.Subject7); 
				$('#Grade7').val(data.Grade7); 

				$('#Subject8').val(data.Subject8); 
				$('#Grade8').val(data.Grade8); 

				$('#Subject9').val(data.Subject9); 
				$('#Grade9').val(data.Grade9);

				$('#Subject10').val(data.Subject10); 
				$('#Grade10').val(data.Grade10);

				$('#Subject11').val(data.Subject11); 
				$('#Grade11').val(data.Grade11); 

				$('#Subject12').val(data.Subject12); 
				$('#Grade12').val(data.Grade12); 

				$('#Subject13').val(data.Subject13); 
				$('#Grade13').val(data.Grade13); 

				$('#Subject14').val(data.Subject14); 
				$('#Grade14').val(data.Grade14); 

				$('#Subject15').val(data.Subject15); 
				$('#Grade15').val(data.Grade15); 

				$('#Subject16').val(data.Subject16); 
				$('#Grade16').val(data.Grade16); 

				$('#Subject17').val(data.Subject17); 
				$('#Grade17').val(data.Grade17); 

				$('#Subject18').val(data.Subject18); 
				$('#Grade18').val(data.Grade18); 

				$('#Subject19').val(data.Subject19); 
				$('#Grade19').val(data.Grade19);

				$('#Subject20').val(data.Subject20); 
				$('#Grade20').val(data.Grade20);

				$('#Subject21').val(data.Subject21); 
				$('#Grade21').val(data.Grade21); 

				$('#Subject22').val(data.Subject22); 
				$('#Grade22').val(data.Grade22); 

				$('#Subject23').val(data.Subject23); 
				$('#Grade23').val(data.Grade23); 

				$('#Subject24').val(data.Subject24); 
				$('#Grade24').val(data.Grade24); 

				$('#Subject25').val(data.Subject25); 
				$('#Grade25').val(data.Grade25); 

				$('#Subject26').val(data.Subject26); 
				$('#Grade26').val(data.Grade26); 

				$('#Subject27').val(data.Subject27); 
				$('#Grade27').val(data.Grade27); 

				$('#Subject28').val(data.Subject28); 
				$('#Grade28').val(data.Grade28); 

				$('#Subject29').val(data.Subject29); 
				$('#Grade29').val(data.Grade29);

				$('#Subject30').val(data.Subject30); 
				$('#Grade30').val(data.Grade30);

				$('#Subject31').val(data.Subject31); 
				$('#Grade31').val(data.Grade31);

				$('#Subject32').val(data.Subject32); 
				$('#Grade32').val(data.Grade32); 

				$('.modal-title').text("Edit Student Grades");
				$('#id').val(id);
				$('#action').val("Save Changes");
				$('#operation').val("Edit");
			}
		})
	});
	
	$(document).on('click', '.delete', function(){
		var id = $(this).attr("id");
		if(confirm("Are you sure you want to delete this?"))
		{
			$.ajax({
				url:"deletegradshs.php",
				method:"POST",
				data:{id:id},
				success:function(data)
				{
					alert(data);
					dataTable.ajax.reload();
				}
			});
		}
		else
		{
			return false;	
		}
	});
	
	
});
</script>