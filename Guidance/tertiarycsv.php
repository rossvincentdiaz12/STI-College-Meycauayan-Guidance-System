<?php
session_start();
error_reporting(0);
	if(isset($_SESSION["username"])==false)// ginagamit yan para di ka makapunta sa 2nd page, need to log in first para makapunta sa next page.
	{
		header("location:index.php");
	}
$connect = mysqli_connect("localhost", "root", "", "guidance_db");
$message = '';
if(isset($_POST["submit"]))
{
	if($_FILES['file']['name'])
	{
		$filename = explode(".", $_FILES['file']['name']);
		if($filename[1] == 'csv')
	{
		$handle = fopen($_FILES['file']['tmp_name'], "r");
		fgetcsv($handle); //hiding first ROW in csv (*Horizontal line*)
		while(($data = fgetcsv($handle, 1000, ",")) !== FALSE) //hiding first ROW in csv (*Horizontal line*)
		{
			$item1 = mysqli_real_escape_string($connect, $data[0]);
			$item2 = mysqli_real_escape_string($connect, $data[1]);
			$item3 = mysqli_real_escape_string($connect, $data[2]);
			$item4 = mysqli_real_escape_string($connect, $data[3]);
			$item5 = mysqli_real_escape_string($connect, $data[4]);
			$item6 = mysqli_real_escape_string($connect, $data[5]);
			$item7 = mysqli_real_escape_string($connect, $data[6]);
			$item8 = mysqli_real_escape_string($connect, $data[7]);
			$item9 = mysqli_real_escape_string($connect, $data[8]);
			$query = "INSERT INTO tbl_students(StudentNumber, Firstname, Middlename, Lastname, Course, Year, Birthday, Gender, Address)VALUES('$item1','$item2','$item3','$item4','$item5','$item6','$item7,'$item8','$item9')";
			mysqli_query($connect, $query);
		}
		fclose($handle);
		header("location: tertiarycsv.php?add=");
		include 'errors.php';
		echo "<script>alert('Imported');</script>";
		}
		else
		{
			$message = '<label class="text-danger">Please select .CSV file only.</label>';
		}
	}
	else
	{
		$message = '<label class="text-danger">Please select file</label>';
	}
}

if(isset($_GET["updation"]))
{
	$message = '<label class="text-danger">Student Updation Done</label>';
}
$query = "SELECT * FROM tbl_students";
$result = mysqli_query($connect, $query);

?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Upload File | Tertiary Students</title>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
		<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
		<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>		
		<link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
   	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Cookie">
    <link rel="stylesheet" href="assets/fonts/font-awesome.min.css">
    <link rel="stylesheet" href="assets/fonts/material-icons.css">
    <link rel="stylesheet" href="assets/css/styles.css">

	</head>
	<body>
		<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header"><a href="tertiarycsv.php" class="navbar-brand navbar-link"><strong>STI College Meycauayan</strong>     Tertiary Students</a>
            <button data-toggle="collapse" data-target="#navcol-1" class="navbar-toggle collapsed"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
        </div>

        <div class="collapse navbar-collapse" id="navcol-1">
            
                
                <ul class="nav navbar-nav navbar-right">
                <li role="presentation"><a href="indexannounce.php">Announcements </a></li>
                <li class="dropdown"><a data-toggle="dropdown" aria-expanded="false" href="#" class="dropdown-toggle">Upload CSV File<span class="caret"></span></a>
                    <ul role="menu" class="dropdown-menu">
                        <li role="presentation"><a href="tertiarycsv.php">Tertiary Students</a></li>
                        <li role="presentation"><a href="seniorcsv.php">Senior High Students</a></li>
                        
                    </ul>
                </li>

                <li class="dropdown"><a data-toggle="dropdown" aria-expanded="false" href="#" class="dropdown-toggle">Grades <span class="caret"></span></a>
                    <ul role="menu" class="dropdown-menu">
                        <li role="presentation"><a href="indexgradter.php">Tertiary Students</a></li>
                        <li role="presentation"><a href="indexgradshs.php">Senior High Students</a></li>
                    </ul>

                    <li class="dropdown"><a data-toggle="dropdown" aria-expanded="false" href="#" class="dropdown-toggle">Offenses <span class="caret"></span></a>
                <ul role="menu" class="dropdown-menu">
                    <li role="presentation"><a href="indexteroff.php">Tertiary Students</a></li>
                    <li role="presentation"><a href="indexshsoff.php">Senior High Students</a></li>
                </ul>

                <li class="dropdown"><a data-toggle="dropdown" aria-expanded="false" href="#" class="dropdown-toggle">About STI<span class="caret"></span></a>
                    <ul role="menu" class="dropdown-menu">
                        <li role="presentation"><a href="history.php">History</a></li>
                        <li role="presentation"><a href="vision.php">Vision, Mission and Hymn</a></li>
                        
                    </ul>
                </li>
                
                <li class="dropdown"><a data-toggle="dropdown" aria-expanded="false" href="#" class="dropdown-toggle">
                	<?php echo $_SESSION['username']; ?><span class="caret"></span></a>
                    <ul role="menu" class="dropdown-menu">
                        <li role="presentation"><a href="updateadmin.php">Edit Profile</a></li>
                        <li role="presentation"><a href="addusers.php">Add Users</a></li>
                        <li role="presentation"><a href="addoffenses.php">Add Offenses</a></li>
                        <li role="presentation"><a href="index.php?action=logout">Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav><br><br><br>
	<br />
	<div class="col-md-12"> <!-- Malaking box na kulay blue Students account simula  -->
    <div class="panel panel-primary">
      <div class="panel-heading" style="color:yellow;">Lists of Students</div>
      <div class="panel-body">
        <div class="col-md-12"> <!-- hanggang dito  -->

	<div class="container">
	<br />
	<form method="post" enctype="multipart/form-data">
		<p><label>Please select .CSV file only.</label>
			<input type="file" name="file" /></p>
			<?php echo $message; ?> <br/>
			<input type="submit" name="submit" value="Import" class="btn btn-info" />
			<a href="indexannounce.php?action=back" class="btn btn-info">Back</a> 
			
		</div>
	</form>
			<div class="table-responsive">
				<table class="table table-bordered table-striped">
					<tr>
			<div class="table-responsive">
				<br />
				<div align="right">
					<button type="button" id="add_button" data-toggle="modal" data-target="#userModal" class="btn btn-info">Add Student</button>
				</div>
				<table id="user_data" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>Student Number</th>
							<th>First Name</th>
							<th>Middle Name</th>
							<th>Lastname</th>
							<th>Course</th>
							<th>Year</th>
							<th>Birthday</th>
							<th>Gender</th>
							<th>Address</th>
							<th>Edit</th>
							<th>Delete</th>
						</tr>
					</thead>
				</table>
				
			</div>
		</div>
		</div> <!-- closing tag ng malaking box na student accounts simula  -->
</div>
</div>
</div>
</div> <!-- hanggang dito  -->

	</body>
</html>

<div id="userModal" class="modal fade">
	<div class="modal-dialog">
		<form method="post" id="user_form" enctype="multipart/form-data">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Add User</h4>
				</div>
				<div class="modal-body">
					<label>Enter Student Number</label>
					<input type="text" name="StudentNumber" id="StudentNumber" class="form-control" />
					<br />
					<label>Enter First name</label>
					<input type="text" name="Firstname" id="Firstname" class="form-control" />
					<br />
					<label>Enter Middlename</label>
					<input type="text" name="Middlename" id="Middlename" class="form-control" />
					<br />
					<label>Enter Last Name</label>
					<input type="text" name="Lastname" id="Lastname" class="form-control" />
					<br />
					<label>Enter Course</label>
					<select name="Course" id="Course" class="form-control" /> 
						  <option value="BSTM">BS Tourism Management (BSTM)</option>
						  <option value="BSHRM">BS Hotel Restaurant Management (BSHRM)</option>
						  <option value="BSTM">BS Tourism Management (BSTM)</option>
						  <option value="BSAIS">BS Accounting Information System (BSAIS)</option>
						  <option value="BSHM">BS Hospitality Management (BSHM)</option>
						  <option value="BSIS">BS Information Systems (BSIS)</option>
						  <option value="BSIT">BS Information Technology (BSIT)</option>
						  <option value="BSCpE">BS Computer Engineering (BSCpE)</option>
					</select>
					<br />
					<label>Enter Year</label>
					<select name="Year" id="Year" class="form-control" />
						<option value="11A">1st year(1st Term)</option>
					 	<option value="11B">1st year(2nd Term)</option>
					  	<option value="21A">2nd year(1st Term)</option>
					  	<option value="21B">2nd year(2nd Term)</option>
					  	<option value="31A">3rd year(1st Term)</option>
					  	<option value="31B">3rd year(2nd Term)</option>
					  	<option value="41A">4th year(1st Term)</option>
					  	<option value="41B">4th year(2nd Term)</option>
					</select>
					<br />
					<label>Enter Birthday</label>
					<input type="date" name="Birthday" id="Birthday" class="form-control" />
					<br />
					<label>Enter Gender</label>
					<select name="Gender" id="Gender" class="form-control" />
						<option value="Male">Male</option>
						<option value="Female">Female</option>
					</select>
					<br />
					<label>Enter Address</label>
					<input type="text" name="Address" id="Address" class="form-control" />
					<br />
					<br />
					<label>Enter Password</label>
					<input type="text" name="Password" id="Password" class="form-control" />
					<br />
					
				</div>
				<div class="modal-footer">
					<input type="hidden" name="id" id="id" />
					<input type="hidden" name="operation" id="operation" />
					<input type="submit" name="action" id="action" class="btn btn-success" value="Add" />
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</form>
	</div>
</div>

<script type="text/javascript" language="javascript" >
$(document).ready(function(){
	$('#add_button').click(function(){
		$('#user_form')[0].reset();
		$('.modal-title').text("Add Tertiary Student");
		$('#action').val("Insert");
		$('#operation').val("Add");
	});
	
	var dataTable = $('#user_data').DataTable({
		"processing":true,
		"serverSide":true,
		"order":[],
		"ajax":{
			url:"fetchtertiary.php",
			type:"POST"
		},
		"columnDefs":[
			{
				"targets":[0],
				"orderable":false,
			},
		],

	});

	$(document).on('submit', '#user_form', function(event){
		event.preventDefault();
		var StudentNumber = $('#StudentNumber').val();
		var Firstname = $('#Firstname').val();
		var Middlename = $('#Middlename').val();
		var Lastname = $('#Lastname').val();
		var Course = $('#Course').val();
		var Year = $('#Year').val();
		var Birthday = $('#Birthday').val();
		var Gender = $('#Gender').val();
		var Address = $('#Address').val();
		if(StudentNumber != '' && Firstname != '' && Middlename != '' && Lastname != '' && Course != '' && Year != '' && Birthday != '' && Gender != '' && Address != '' && Password != '')
		{
			$.ajax({
				url:"inserttertiary.php",
				method:'POST',
				data:new FormData(this),
				contentType:false,
				processData:false,
				success:function(data)
				{
					alert(data);
					$('#user_form')[0].reset();
					$('#userModal').modal('hide');
					dataTable.ajax.reload();
				}
			});
		}
		else
		{
			alert("All Fields are Required");
		}
	});
	
	$(document).on('click', '.update', function(){
		var id = $(this).attr("id");
		$.ajax({
			url:"fetch_singletertiary.php",
			method:"POST",
			data:{id:id},
			dataType:"json",
			success:function(data)
			{
				$('#userModal').modal('show');
				$('#StudentNumber').val(data.StudentNumber);
				$('#Firstname').val(data.Firstname);
				$('#Middlename').val(data.Middlename);
				$('#Lastname').val(data.Lastname);
				$('#Course').val(data.Course);
				$('#Year').val(data.Year);
				$('#Birthday').val(data.Birthday);
				$('#Gender').val(data.Gender);
				$('#Address').val(data.Address);
				$('#Password').val(data.Password);
				$('.modal-title').text("Edit Tertiary Student");
				$('#id').val(id);
				$('#action').val("Save Changes");
				$('#operation').val("Edit");
			}
		})
	});
	
	$(document).on('click', '.delete', function(){
		var id = $(this).attr("id");
		if(confirm("Are you sure you want to delete this?"))
		{
			$.ajax({
				url:"deletetertiary.php",
				method:"POST",
				data:{id:id},
				success:function(data)
				{
					dataTable.ajax.reload();
				}
			});
		}
		else
		{
			return false;	
		}
	});
	
	
});
</script>