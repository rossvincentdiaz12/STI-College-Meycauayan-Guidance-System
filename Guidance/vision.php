<?php
session_start();

	if(isset($_SESSION["username"])==false)// ginagamit yan para di ka makapunta sa 2nd page, need to log in first para makapunta sa next page.
	{
		header("location:index.php");
	}
?>
<!DOCTYPE html>
<html>
<title>STI Vision</title>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Untitled</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Cookie">
    <link rel="stylesheet" href="assets/fonts/font-awesome.min.css">
    <link rel="stylesheet" href="assets/fonts/material-icons.css">
    <link rel="stylesheet" href="assets/css/styles.css">
    <link rel="stylesheet" href="assets/css/Pretty-Footer.css">
</head>

<body>
    <nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header"><a href="vision.php" class="navbar-brand navbar-link"><strong>STI College Meycauayan</strong> Vision, Mission and Hymn</a>
            <button data-toggle="collapse" data-target="#navcol-1" class="navbar-toggle collapsed"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
        </div>

        <div class="collapse navbar-collapse" id="navcol-1">
            
                
                <ul class="nav navbar-nav navbar-right">
                <li role="presentation"><a href="indexAnnounce.php">Announcements </a></li>
                <li class="dropdown"><a data-toggle="dropdown" aria-expanded="false" href="#" class="dropdown-toggle">Upload CSV File<span class="caret"></span></a>

                    <ul role="menu" class="dropdown-menu">
                        <li role="presentation"><a href="tertiarycsv.php">Tertiary Students</a></li>
                        <li role="presentation"><a href="seniorcsv.php">Senior High Students</a></li>
                        
                    </ul>
                </li>
                
                <li class="dropdown"><a data-toggle="dropdown" aria-expanded="false" href="#" class="dropdown-toggle">Grades <span class="caret"></span></a>
                    <ul role="menu" class="dropdown-menu">
                        <li role="presentation"><a href="indexgradter.php">Tertiary Students</a></li>
                        <li role="presentation"><a href="indexgradshs.php">Senior High Students</a></li>
                    </ul>
                <li class="dropdown"><a data-toggle="dropdown" aria-expanded="false" href="#" class="dropdown-toggle">Offenses <span class="caret"></span></a>
                <ul role="menu" class="dropdown-menu">
                    <li role="presentation"><a href="indexteroff.php">Tertiary Students</a></li>
                    <li role="presentation"><a href="indexshsoff.php">Senior High Students</a></li>
                </ul>
                <li class="dropdown"><a data-toggle="dropdown" aria-expanded="false" href="#" class="dropdown-toggle">About STI<span class="caret"></span></a>
                    <ul role="menu" class="dropdown-menu">
                        <li role="presentation"><a href="history.php">History</a></li>
                        <li role="presentation"><a href="vision.php">Vision, Mission and Hymn</a></li>
                    </ul>
                </li>

                <li class="dropdown"><a data-toggle="dropdown" aria-expanded="false" href="#" class="dropdown-toggle">
                	<?php echo $_SESSION['username']; ?><span class="caret"></span></a>
                    <ul role="menu" class="dropdown-menu">
                        <li role="presentation"><a href="updateadmin.php">Edit Profile</a></li>
                        <li role="presentation"><a href="addusers.php">Add Users</a></li>
                        <li role="presentation"><a href="addoffenses.php">Add Offenses</a></li>
                        <li role="presentation"><a href="index.php?action=logout">Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav><br><br><br>
    <div>
        <div class="thumbnail"> <img src="assets/img/sticollege.jpg"></div>
    </div>
    <div class="container sti">
        <h1>Vision </h1>
        <p><em>To be the leader in innovative and relevant education that nurtures individuals to become competent and responsible members of society.</em> </p>
    </div>
    <div class="container sti">
        <h1>Mission </h1>
        <p><em>We are an institution committed to provide knowledge through the development and delivery of superior learning systems.We strive to provide optimum value to all our stakeholders – our students, our faculty members, our employees, our partners, our shareholders, and our community.We will pursue this mission with utmost integrity, dedication, transparency, and creativity.</em>            </p>
    </div>
    <div class="container sti">
        <h1>STI Hymn </h1>
        <p><em>Aim high with STI <br>
        The future is here today <br>
        Fly high with STI <br>
        Be the best that you can be <br>
        Onward to tomorrow <br>
        With dignity and pride <br>
        A vision of excellence <br>
        Our resounding battle cry <br>
        Aim high with STI <br>
        The future is here today <br>
        Fly high with STI <br>
        Be the best that you can be <br> </em>            </p>
    </div>


    <div class="dark-section"></div>
    <div class="photos"></div>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
</body>

</html>