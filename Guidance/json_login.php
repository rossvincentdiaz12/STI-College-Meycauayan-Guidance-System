<?php

	if($_SERVER['REQUEST_METHOD'] == 'POST'){

		$studentnumber = $_POST['StudentNumber'];
		$password = $_POST['Password'];

		require_once 'json_connect.php';

		$sql = "SELECT * FROM tbl_students UNION SELECT * FROM tbl_seniorhigh WHERE StudentNumber = '$studentnumber' ";

		$response = mysqli_query($conn, $sql);

		$result = array();
		$result['login'] = array();

		if( mysqli_num_rows($response) === 1 ){

			$row = mysqli_fetch_assoc($response);

			if( password_verify($password, $row['password']) ){

				$index['Lastname'] = $row['Lastname'];
				$index['StudentNumber'] = $row['StudentNumber'];

				array_push($result['login'], $index);

				$result['success'] = "1";
				$result['message'] = "success";
				echo json_encode($result);

				mysqli_close($conn);
			} else {
				$result['success'] = "0";
				$result['message'] = "error";
				echo json_encode($result);

				mysqli_close($conn);
			}
		}
	}
?>