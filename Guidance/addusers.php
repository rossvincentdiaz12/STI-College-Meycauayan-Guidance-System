<?php  
session_start();

  if(isset($_SESSION["username"])==false)// ginagamit yan para di ka makapunta sa 2nd page, need to log in first para makapunta sa next page.
  {
    header("location:index.php");
  }
 $connect = mysqli_connect("localhost", "root", "", "guidance_db");  
 if(isset($_POST["register"]))  
 {  
      if($_POST["Password"] != $_POST["ConfirmPass"]) 
      {  
           echo '<script>alert("Password didnt match")</script>';  
      }  
      else  
      {  
           $Firstname = mysqli_real_escape_string($connect, $_POST["Firstname"]);  
           $Lastname = mysqli_real_escape_string($connect, $_POST["Lastname"]);
           $Username = mysqli_real_escape_string($connect, $_POST["Username"]);  
           $Password = mysqli_real_escape_string($connect, $_POST["Password"]); 
           $ConfirmPass = mysqli_real_escape_string($connect, $_POST["ConfirmPass"]); 
           $query = "INSERT INTO tbl_users (Firstname,Lastname,Username,Password,ConfirmPass) VALUES
           ('$Firstname','$Lastname','$Username', '$Password', '$ConfirmPass')";  
           if(mysqli_query($connect, $query))  
           {  
                echo '<script>alert("Registered Successfully")</script>';  
           }  
      }  
 }  
 
 ?>  
 <!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE-edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Add Users</title>
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/fonts/font-awesome.min.css">
    <link rel="stylesheet" href="assets/fonts/material-icons.css">
    <link rel="stylesheet" href="assets/css/styles.css">
</head>
<body>
    <nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header"><a href="addusers.php" class="navbar-brand navbar-link"><strong>STI College Meycauayan</strong> Admin Profile</a>
            <button data-toggle="collapse" data-target="#navcol-1" class="navbar-toggle collapsed"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
        </div>

        <div class="collapse navbar-collapse" id="navcol-1">
            
                
                <ul class="nav navbar-nav navbar-right">
                <li role="presentation"><a href="indexAnnounce.php">Announcements </a></li>
                <li class="dropdown"><a data-toggle="dropdown" aria-expanded="false" href="#" class="dropdown-toggle">Upload CSV File<span class="caret"></span></a>
                    <ul role="menu" class="dropdown-menu">
                        <li role="presentation"><a href="tertiarycsv.php">Tertiary Students</a></li>
                        <li role="presentation"><a href="seniorcsv.php">Senior High Students</a></li>
                        
                    </ul>
                </li>
                
                <li class="dropdown"><a data-toggle="dropdown" aria-expanded="false" href="#" class="dropdown-toggle">Grades <span class="caret"></span></a>
                    <ul role="menu" class="dropdown-menu">
                        <li role="presentation"><a href="indexgradter.php">Tertiary Students</a></li>
                        <li role="presentation"><a href="indexgradshs.php">Senior High Students</a></li>
                    </ul>

                <li class="dropdown"><a data-toggle="dropdown" aria-expanded="false" href="#" class="dropdown-toggle">Offenses<span class="caret"></span></a>
                <ul role="menu" class="dropdown-menu">
                    <li role="presentation"><a href="indexteroff.php">Tertiary Students</a></li>
                    <li role="presentation"><a href="indexshsoff.php">Senior High Students</a></li>
                </ul>
                <li class="dropdown"><a data-toggle="dropdown" aria-expanded="false" href="#" class="dropdown-toggle">About STI<span class="caret"></span></a>
                    <ul role="menu" class="dropdown-menu">
                        <li role="presentation"><a href="history.php">History</a></li>
                        <li role="presentation"><a href="vision.php">Vision, Mission and Hymn</a></li>
                        
                    </ul>
                </li>

                <li class="dropdown"><a data-toggle="dropdown" aria-expanded="false" href="#" class="dropdown-toggle">
                  <?php echo $_SESSION['username']; ?><span class="caret"></span></a>
                    <ul role="menu" class="dropdown-menu">
                        <li role="presentation"><a href="updateadmin.php">Edit Profile</a></li>
                        <li role="presentation"><a href="addusers.php">Add Users</a></li>
                        <li role="presentation"><a href="addoffenses.php">Add Offenses</a></li>
                        <li role="presentation"><a href="index.php?action=logout">Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav><br><br><br>
  <br />
  <div class="col-md-12"> <!-- Malaking box na kulay blue Students account simula  -->
    <div class="panel panel-primary">
      <div class="panel-heading" style="color:yellow;">Add Administrator</div>
      <div class="panel-body">
        <div class="col-md-12"> <!-- hanggang dito  -->

<p><br/><br/></p>

<div class="container">
    <div class="row">
      <div class="col-md-3">
    
      </div>
      <div class="col-md-6">
        <div class="panel panel-default">
          <div class="panel-body">

                <form method="post">  
                     <label>Firstname</label>  
                     <input type="text" name="Firstname" class="form-control" required="" />  
                     <br />  
                     <label>Lastname</label>  
                     <input type="text" name="Lastname" class="form-control" required=""/>  
                     <br />  
                     <label>Username</label>  
                     <input type="text" name="Username" class="form-control" required=""/>  
                     <br />  
                     <label>Password</label>  
                     <input type="Password" name="Password" class="form-control" required=""/>  
                     <br />  
                     <label>Confirm Password</label>  
                     <input type="Password" name="ConfirmPass" class="form-control" required=""/>  
                     <br />  
                     <input type="submit" name="register" value="Register" class="btn btn-info" />
                     <button type="button" onclick="location.href='welcome.php'" class="btn btn-info">Back</button>
                     <br />  
                </form>  
              </div>
            </div>
          </div>
                <div class="col-md-3">
                  </div>
  </div>
</div>
                <p><br/></p>
  <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.dataTables.min.js"></script>
    <script src="js/dataTables.bootstrap.min.js"></script>
    
           </div>  
           </div> <!-- closing tag ng malaking box na student accounts simula  -->
</div>
</div>
</div>
</div> <!-- hanggang dito  -->
      </body>  
 </html>