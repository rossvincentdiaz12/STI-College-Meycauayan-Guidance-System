<?php
include('db.php');
include('functiontertiary.php');
if(isset($_POST["operation"]))
{
	if($_POST["operation"] == "Add")
	{
		$statement = $connection->prepare("
			INSERT INTO tbl_students (StudentNumber, 
										Firstname, 
										Middlename, 
										Lastname, 
										Course, 
										Year, 
										Birthday, 
										Gender, 
										Address,
										Password) 
							VALUES (:StudentNumber, 
									:Firstname, 
									:Middlename, 
									:Lastname, 
									:Course, 
									:Year, 
									:Birthday, 
									:Gender, 
									:Address,
									:Password)
		");
		$result = $statement->execute(
			array(
				':StudentNumber'	=>	$_POST["StudentNumber"],
				':Firstname'		=>	$_POST["Firstname"],
				':Middlename'		=>	$_POST["Middlename"],
				':Lastname'			=>	$_POST["Lastname"],
				':Course'			=>	$_POST["Course"],
				':Year'				=>	$_POST["Year"],
				':Birthday'			=>	$_POST["Birthday"],
				':Gender'			=>	$_POST["Gender"],
				':Address'			=>	$_POST["Address"],
				':Password'			=>	$_POST["Password"]
			)
		);
		if(!empty($result))
		{
			echo 'Data Inserted';
		}
	}
	if($_POST["operation"] == "Edit")
	{
		$statement = $connection->prepare(
			"UPDATE tbl_students 
			SET StudentNumber = :StudentNumber, 
				Firstname = :Firstname,
				Middlename = :Middlename, 
				Lastname = :Lastname, 
				Course = :Course, 
				Year = :Year, 
				Birthday = :Birthday, 
				Gender = :Gender,
				Address = :Address,
				Password = :Password
			WHERE id = :id
			"
		);
		$result = $statement->execute(
			array(
				':StudentNumber'	=>	$_POST["StudentNumber"],
				':Firstname'		=>	$_POST["Firstname"],
				':Middlename'		=>	$_POST["Middlename"],
				':Lastname'			=>	$_POST["Lastname"],
				':Course'			=>	$_POST["Course"],
				':Year'				=>	$_POST["Year"],
				':Birthday'			=>	$_POST["Birthday"],
				':Gender'			=>	$_POST["Gender"],
				':Address'			=>	$_POST["Address"],
				':Password'			=>	$_POST["Password"],
				':id'				=>	$_POST["id"]
			)
		);
		if(!empty($result))
		{
			echo 'Data Updated';
		}
	}
}
?>