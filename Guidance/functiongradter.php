<?php

function get_total_all_records()
{
	include('db.php');
	$statement = $connection->prepare("SELECT StudentNumber,Lastname,Course,Year FROM tbl_students");
	$statement->execute();
	$result = $statement->fetchAll();
	return $statement->rowCount();
}

?>