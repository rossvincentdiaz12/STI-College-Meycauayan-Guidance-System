<?php
session_start();

	if(isset($_SESSION["username"])==false)// ginagamit yan para di ka makapunta sa 2nd page, need to log in first para makapunta sa next page.
	{
		header("location:index.php");
	}
$connect = mysqli_connect("localhost", "root", "", "guidance_db");

$query = "SELECT * FROM tbl_announcements";
$result = mysqli_query($connect, $query);
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>STI | Announcements</title>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
		<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
		<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>		
		<link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
   	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Cookie">
    <link rel="stylesheet" href="assets/fonts/font-awesome.min.css">
    <link rel="stylesheet" href="assets/fonts/material-icons.css">
    <link rel="stylesheet" href="assets/css/styles.css">

	</head>
	<body>
		<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header"><a href="indexannounce.php" class="navbar-brand navbar-link"><strong>STI College Meycauayan</strong>     Announcements</a>
            <button data-toggle="collapse" data-target="#navcol-1" class="navbar-toggle collapsed"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
        </div>

        <div class="collapse navbar-collapse" id="navcol-1">
            
                
                <ul class="nav navbar-nav navbar-right">
                <li role="presentation" class="active"><a href="indexAnnounce.php">Announcements </a></li>
                <li class="dropdown"><a data-toggle="dropdown" aria-expanded="false" href="#" class="dropdown-toggle">Upload CSV File<span class="caret"></span></a>
                    <ul role="menu" class="dropdown-menu">
                        <li role="presentation"><a href="tertiarycsv.php">Tertiary Students</a></li>
                        <li role="presentation"><a href="seniorcsv.php">Senior High Students</a></li>
                        
                    </ul>
                </li>
                
                <li class="dropdown"><a data-toggle="dropdown" aria-expanded="false" href="#" class="dropdown-toggle">Grades <span class="caret"></span></a>
                    <ul role="menu" class="dropdown-menu">
                        <li role="presentation"><a href="indexgradter.php">Tertiary Students</a></li>
                        <li role="presentation"><a href="indexgradshs.php">Senior High Students</a></li>
                    </ul>

                    <li class="dropdown"><a data-toggle="dropdown" aria-expanded="false" href="#" class="dropdown-toggle">Offenses <span class="caret"></span></a>
                <ul role="menu" class="dropdown-menu">
                    <li role="presentation"><a href="indexteroff.php">Tertiary Students</a></li>
                    <li role="presentation"><a href="indexshsoff.php">Senior High Students</a></li>
                </ul>

                <li class="dropdown"><a data-toggle="dropdown" aria-expanded="false" href="#" class="dropdown-toggle">About STI<span class="caret"></span></a>
                    <ul role="menu" class="dropdown-menu">
                        <li role="presentation"><a href="history.php">History</a></li>
                        <li role="presentation"><a href="vision.php">Vision, Mission and Hymn</a></li>
                        
                    </ul>
                </li>
                
                <li class="dropdown"><a data-toggle="dropdown" aria-expanded="false" href="#" class="dropdown-toggle">
                	<?php echo $_SESSION['username']; ?><span class="caret"></span></a>
                    <ul role="menu" class="dropdown-menu">
                        <li role="presentation"><a href="updateadmin.php">Edit Profile</a></li>
                        <li role="presentation"><a href="addusers.php">Add Users</a></li>
                        <li role="presentation"><a href="addoffenses.php">Add Offenses</a></li>
                        <li role="presentation"><a href="index.php?action=logout">Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav><br><br><br>
	<br />
	<div class="col-md-12"> <!-- Malaking box na kulay blue Students account simula  -->
    <div class="panel panel-primary">
      <div class="panel-heading" style="color:yellow;">Announcements</div>
      <div class="panel-body">
        <div class="col-md-12"> <!-- hanggang dito  -->

	</form>
			<div class="table-responsive">
				<table class="table table-bordered table-striped">
					<tr>
			<div class="table-responsive">
				<br />
				<div align="right">
					<button type="button" id="add_button" data-toggle="modal" data-target="#userModal" class="btn btn-info">Add Announcement</button>
				</div>
				<table id="user_data" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>Title</th>
							<th>Date</th>
							<th>Description</th>
							<th width="10%">Edit</th>
							<th width="10%">Delete</th>
						</tr>
					</thead>
				</table>
				
			</div>
		</div>
		</div> <!-- closing tag ng malaking box na student accounts simula  -->
</div>
</div>
</div>
</div> <!-- hanggang dito  -->

	</body>
</html>

<div id="userModal" class="modal fade">
	<div class="modal-dialog">
		<form method="post" id="user_form" enctype="multipart/form-data">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Add Announcement</h4>
				</div>
				<div class="modal-body">
					<label>Title</label>
					<input type="text" name="Title" id="Title" class="form-control" />
					<br />
					<label>Date Posted</label>
					<input type="Date" name="AnnouncementDate" id="AnnouncementDate" class="form-control" />
					<br />
					<label>Post Until</label>
					<input type="Date" name="DateUntil" id="DateUntil" class="form-control" />
					<br />
					<label>Description</label>
					<textarea type="text" name="Details" id="Details" class="form-control" /></textarea>
					<br />
					
					
				</div>
				<div class="modal-footer">
					<input type="hidden" name="id" id="id" />
					<input type="hidden" name="operation" id="operation" />
					<input type="submit" name="action" id="action" class="btn btn-success" value="Add" />
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</form>
	</div>
</div>

<script type="text/javascript" language="javascript" >
$(document).ready(function(){
	$('#add_button').click(function(){
		$('#user_form')[0].reset();
		$('.modal-title').text("Add Announcement");
		$('#action').val("Post");
		$('#operation').val("Add");
	});
	
	var dataTable = $('#user_data').DataTable({
		"processing":true,
		"serverSide":true,
		"order":[],
		"ajax":{
			url:"fetchannounce.php",
			type:"POST"
		},
		"columnDefs":[
			{
				"targets":[0],
				"orderable":false,
			},
		],

	});

	$(document).on('submit', '#user_form', function(event){
		event.preventDefault();
		var Title = $('#Title').val();
		var AnnouncementDate = $('#AnnouncementDate').val();
		var Details = $('#Details').val();
		if(Title != '' && AnnouncementDate != '' && Details != '')
		{
			$.ajax({
				url:"insertannounce.php",
				method:'POST',
				data:new FormData(this),
				contentType:false,
				processData:false,
				success:function(data)
				{
					alert(data);
					$('#user_form')[0].reset();
					$('#userModal').modal('hide');
					dataTable.ajax.reload();
				}
			});
		}
		else
		{
			alert("All Fields are Required");
		}
	});
	
	$(document).on('click', '.update', function(){
		var id = $(this).attr("id");
		$.ajax({
			url:"fetch_singleannounce.php",
			method:"POST",
			data:{id:id},
			dataType:"json",
			success:function(data)
			{
				$('#userModal').modal('show');
				$('#Title').val(data.Title);
				$('#AnnouncementDate').val(data.AnnouncementDate);
				$('#Details').val(data.Details);
				$('.modal-title').text("Edit Announcement");
				$('#id').val(id);
				$('#action').val("Save Changes");
				$('#operation').val("Edit");
			}
		})
	});
	
	$(document).on('click', '.delete', function(){
		var id = $(this).attr("id");
		if(confirm("Are you sure you want to delete this announcement?"))
		{
			$.ajax({
				url:"deleteannounce.php",
				method:"POST",
				data:{id:id},
				success:function(data)
				{
					dataTable.ajax.reload();
				}
			});
		}
		else
		{
			return false;	
		}
	});
	
	
});
</script>