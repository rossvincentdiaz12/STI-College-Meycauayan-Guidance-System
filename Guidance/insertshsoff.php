<?php
include('db.php');
include('functionshsoff.php');
if(isset($_POST["operation"]))
{
	if($_POST["operation"] == "Edit")
	{
		$statement = $connection->prepare(
			"UPDATE tbl_seniorhigh 
			SET 
				Offense1 = :Offense1,
				Date1 = :Date1,
				Remarks1 = :Remarks1
			WHERE id = :id
			"
		);
		$result = $statement->execute(
			array(
				':Offense1'			=>	$_POST["Offense1"],
				':Date1'			=>	$_POST["Date1"],
				':Remarks1'			=>	$_POST["Remarks1"],
				':id'				=>	$_POST["id"]
			)
		);
		if(!empty($result))
		{
			echo 'Data Updated';
		}
	}
}
?>