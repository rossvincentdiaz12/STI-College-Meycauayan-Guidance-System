<?php 
	session_start();
	if(isset($_SESSION['username']))
	{
		unset($_SESSION['username']);
		unset($_SESSION['password']);
	}
	
	$pageWasRefreshed = isset($_SERVER['HTTP_CACHE_CONTROL']) && $_SERVER['HTTP_CACHE_CONTROL'] === 'max-age=0';
	if($_SERVER['REQUEST_METHOD'] === 'POST')
	{
		//location of database , dbconn variable for connection 
		require ('connection.php'); 
				
		// username and password sent to php var
		$username=$_POST['username'];
		$password=$_POST['password'];
				
		// To protect MySQL injection (more detail about MySQL injection)
		$username = stripslashes($username);
		$password = stripslashes($password);
		$username = mysqli_real_escape_string($dbconn,$username);
		$password = mysqli_real_escape_string($dbconn,$password);
			
		$query = "SELECT * FROM tbl_users 
		WHERE username = '$username' and password = '$password'";
		$result = $dbconn -> query($query);

		if($password !== str_replace(' ','',$password))
		{
			echo "<script>
			alert('Invalid Username or Password!');
			window.location='index.php';
			</script>";
		}
		else if($result -> num_rows > 0)
		{
			
			while($row = $result -> fetch_assoc())
			{
				$_SESSION['username'] = $username;
				$_SESSION['password'] = $password;
				
				header("location:indexAnnounce.php");
				
			}		
		}
		else 
		{			
			echo "<script>
			alert('Invalid Username or Password!');
			window.location='index.php';
			</script>";	
		}
		mysqli_close($dbconn);
	}
	
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="CoreUI - Open Source Bootstrap Admin Template">
    <meta name="author" content="Łukasz Holeczek">
    <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
	
    <title>Admin Login</title>
    <!-- Icons-->
    <link href="node_modules/@coreui/icons/css/coreui-icons.min.css" rel="stylesheet">
    <link href="node_modules/flag-icon-css/css/flag-icon.min.css" rel="stylesheet">
    <link href="node_modules/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="node_modules/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
    <!-- Main styles for this application-->
    <link href="src/css/style.css" rel="stylesheet">
    <link href="vendors/pace-progress/css/pace.min.css" rel="stylesheet">
	 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta/css/bootstrap.min.css'><link rel="stylesheet" href="./style.css">
	
	<style>
.card {
  box-shadow: 0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);
}

h4{
	
	color:#4582EC;
}
</style>



  </head>
  <body class="app flex-row align-items-center">
    <div class="container">
    	<form action="index.php" method="post">
		
		
		</br>
		</br>
<div class="container pt-3">
  <div class="row justify-content-sm-center">
    <div class="col-sm-6 col-md-4">

      <div class="card border-info text-center">
        <div class="card-header">
          Sign in to continue
        </div>
        <div class="card-body">
         
          <h4 class="text-center">STI College Meycauayan</h4>
		  </br>
          <form class="form-signin">
            <input type="text" class="form-control mb-2" name="username" placeholder="Enter Username" required autofocus>
            <input type="password" class="form-control mb-2" name="password" placeholder="Password" required></br>
            <button class="btn btn-lg btn-primary btn-block mb-1" name="btnLogin" class="btn" type="submit">Sign in</button></br>
            <label class="checkbox float-left">
              <input type="checkbox" value="remember-me">
              Remember me
            </label>
            <a href="#" class="float-right">Need help?</a>
          </form>
        </div>
      </div>
     
    </div>
  </div>
</div>
</br></br></br></br>
<hr>
<a href="https://gitlab.com/rossvincentdiaz12" class="float-right">Developer</a>
    
    </div>
  </div>
</div>
     
	  
	  
	
  	</form>
    </div>
    <!-- CoreUI and necessary plugins-->
    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/popper.js/dist/umd/popper.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="node_modules/pace-progress/pace.min.js"></script>
    <script src="node_modules/perfect-scrollbar/dist/perfect-scrollbar.min.js"></script>
    <script src="node_modules/@coreui/coreui/dist/js/coreui.min.js"></script>
  </body>

</html>