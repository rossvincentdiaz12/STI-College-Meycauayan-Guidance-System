<?php
include('db.php');
include('functionannounce.php');
if(isset($_POST["operation"]))
{
	if($_POST["operation"] == "Add")
	{
		$statement = $connection->prepare("
			INSERT INTO tbl_announcements (Title, 
											AnnouncementDate, 
											Details) 
							VALUES (:Title, 
									:AnnouncementDate, 
									:Details)
		");
		$result = $statement->execute(
			array(
				':Title'				=>	$_POST["Title"],
				':AnnouncementDate'		=>	$_POST["AnnouncementDate"],
				':Details'				=>	$_POST["Details"]
			)
		);
		if(!empty($result))
		{
			echo 'Announcement Added';
		}
	}
	if($_POST["operation"] == "Edit")
	{
		$statement = $connection->prepare(
			"UPDATE tbl_announcements 
			SET Title = :Title, 
				AnnouncementDate = :AnnouncementDate,
				Details = :Details
			WHERE id = :id
			"
		);
		$result = $statement->execute(
			array(
				':Title'				=>	$_POST["Title"],
				':AnnouncementDate'		=>	$_POST["AnnouncementDate"],
				':Details'				=>	$_POST["Details"],
				':id'					=>	$_POST["id"]
			)
		);
		if(!empty($result))
		{
			echo 'Data Updated';
		}
	}
}
?>