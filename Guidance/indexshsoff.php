<?php
session_start();

	if(isset($_SESSION["username"])==false)// ginagamit yan para di ka makapunta sa 2nd page, need to log in first para makapunta sa next page.
	{
		header("location:index.php");
	}
$connect = mysqli_connect("localhost", "root", "", "guidance_db");

$query = "SELECT StudentNumber,Lastname,Firstname,Course,Year FROM tbl_seniorhigh";
$result = mysqli_query($connect, $query);
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Offenses | SHS</title>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
		<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
		<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>		
		<link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
   	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Cookie">
    <link rel="stylesheet" href="assets/fonts/font-awesome.min.css">
    <link rel="stylesheet" href="assets/fonts/material-icons.css">
    <link rel="stylesheet" href="assets/css/styles.css">

	</head>
	<body>
		<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header"><a href="indexshsoff.php" class="navbar-brand navbar-link"><strong>STI College Meycauayan</strong>     Senior High Students</a>
            <button data-toggle="collapse" data-target="#navcol-1" class="navbar-toggle collapsed"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
        </div>

        <div class="collapse navbar-collapse" id="navcol-1">
            
                
                <ul class="nav navbar-nav navbar-right">
                <li role="presentation"><a href="indexannounce.php">Announcements </a></li>
                <li class="dropdown"><a data-toggle="dropdown" aria-expanded="false" href="#" class="dropdown-toggle">Upload CSV File<span class="caret"></span></a>
                    <ul role="menu" class="dropdown-menu">
                        <li role="presentation"><a href="tertiarycsv.php">Tertiary Students</a></li>
                        <li role="presentation"><a href="seniorcsv.php">Senior High Students</a></li>
                        
                    </ul>
                </li>
                
                <li class="dropdown"><a data-toggle="dropdown" aria-expanded="false" href="#" class="dropdown-toggle">Grades <span class="caret"></span></a>
                    <ul role="menu" class="dropdown-menu">
                        <li role="presentation"><a href="indexgradter.php">Tertiary Students</a></li>
                        <li role="presentation"><a href="indexgradshs.php">Senior High Students</a></li>
                    </ul>

                    <li class="dropdown"><a data-toggle="dropdown" aria-expanded="false" href="#" class="dropdown-toggle">Offenses <span class="caret"></span></a>
                <ul role="menu" class="dropdown-menu">
                    <li role="presentation"><a href="indexteroff.php">Tertiary Students</a></li>
                    <li role="presentation"><a href="indexshsoff.php">Senior High Students</a></li>
                </ul>

                <li class="dropdown"><a data-toggle="dropdown" aria-expanded="false" href="#" class="dropdown-toggle">About STI<span class="caret"></span></a>
                    <ul role="menu" class="dropdown-menu">
                        <li role="presentation"><a href="history.php">History</a></li>
                        <li role="presentation"><a href="vision.php">Vision, Mission and Hymn</a></li>
                        
                    </ul>
                </li>
                
                <li class="dropdown"><a data-toggle="dropdown" aria-expanded="false" href="#" class="dropdown-toggle">
                	<?php echo $_SESSION['username']; ?><span class="caret"></span></a>
                    <ul role="menu" class="dropdown-menu">
                        <li role="presentation"><a href="updateadmin.php">Edit Profile</a></li>
                        <li role="presentation"><a href="addusers.php">Add Users</a></li>
                        <li role="presentation"><a href="addoffenses.php">Add Offenses</a></li>
                        <li role="presentation"><a href="index.php?action=logout">Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav><br><br><br>
	<br />
	<div class="col-md-12"> <!-- Malaking box na kulay blue Students account simula  -->
    <div class="panel panel-primary">
      <div class="panel-heading" style="color:yellow;">Lists of Students</div>
      <div class="panel-body">
        <div class="col-md-12"> <!-- hanggang dito  -->

	
			
			<div class="table-responsive">
				<table class="table table-bordered table-striped">
					<tr>
			<div class="table-responsive">
				<br />
				<div align="right">
					<a href="welcome.php?action=back" class="btn btn-info">Back</a> 
				</div>
				<table id="user_data" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>Student Number</th>
							<th>Lastname</th>
							<th>First Name</th>
							<th>Course</th>
							<th>Year</th>
							<th>Edit</th>
							<th>Delete</th>
						</tr>
					</thead>
				</table>
				
			</div>
		</div>
		</div> <!-- closing tag ng malaking box na student accounts simula  -->
</div>
</div>
</div>
</div> <!-- hanggang dito  -->

	</body>
</html>

<div id="userModal" class="modal fade">
	<div class="modal-dialog">
		<form method="post" id="user_form" enctype="multipart/form-data">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Add Student</h4>
				</div>
				<div class="modal-body">
					<div class="table-responsive">
						
					
				<div class="table-responsive">
						<table class="table table-bordered">
							<tbody>
									<h4><label>Offense </label></h4>
									<tr>
									<?php
									$connect = mysqli_connect("localhost", "root", "", "guidance_db");

									$query = "SELECT * FROM tbl_offenses";

									$result1 = mysqli_query($connect,$query);
									?>

									<td width="70%"><label>Offense Title</label><select name="Offense1" id="Offense1" class="form-control"> <?php while($row1 = mysqli_fetch_array($result1)):;?> 
									
									<option>
									<?php echo $row1[1];  ?>
									<?php endwhile; ?>
									</option>	
									
								
								</td>
								</tr>
								<tr>
									<td width="30%"><label>Date</label><input type="date" name="Date1" id="Date1" class="form-control"/></td>
									</td>
								</tr>
								<tr>
									<td ><label>Remarks</label><textarea name="Remarks1" id="Remarks1" class="form-control" placeholder="First Offense" /></textarea>
								</tr>
							</tbody>
						</table>
					</div>

					
					 
				<div class="modal-footer">
					<input type="hidden" name="id" id="id" />
					<input type="hidden" name="operation" id="operation" />
					<input type="submit" name="action" id="action" class="btn btn-success" value="Add" />
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</form>
	</div>
</div>

<script type="text/javascript" language="javascript" >
$(document).ready(function(){
	$('#add_button').click(function(){
		$('#user_form')[0].reset();
		$('.modal-title').text("Add Student Offense");
		$('#action').val("Insert");
		$('#operation').val("Add");
	});
	
	var dataTable = $('#user_data').DataTable({
		"processing":true,
		"serverSide":true,
		"order":[],
		"ajax":{
			url:"fetchshsoff.php",
			type:"POST"
		},
		"columnDefs":[
			{
				"targets":[0],
				"orderable":false,
			},
		],

	});

	$(document).on('submit', '#user_form', function(event){
		event.preventDefault();
		var StudentNumber = $('#StudentNumber').val();
		var Firstname = $('#Firstname').val();
		var Lastname = $('#Lastname').val();
		var Course = $('#Course').val();
		var Year = $('#Year').val();
		if(StudentNumber != '' && Lastname != '' && Firstname != ''  && Course != '' && Year != '')
		{
			$.ajax({
				url:"insertshsoff.php",
				method:'POST',
				data:new FormData(this),
				contentType:false,
				processData:false,
				success:function(data)
				{
					alert(data);
					$('#user_form')[0].reset();
					$('#userModal').modal('hide');
					dataTable.ajax.reload();
				}
			});
		}
		else
		{
			alert("All Fields are Required");
		}
	});
	
	$(document).on('click', '.update', function(){
		var id = $(this).attr("id");
		$.ajax({
			url:"fetch_singleshsoff.php",
			method:"POST",
			data:{id:id},
			dataType:"json",
			success:function(data)
			{
				$('#userModal').modal('show');
				$('#StudentNumber').val(data.StudentNumber);
				$('#Lastname').val(data.Lastname);
				$('#Firstname').val(data.Firstname);
				$('#Course').val(data.Course);
				$('#Year').val(data.Year);

				$('#Offense1').val(data.Offense1);
				$('#Date1').val(data.Date1);
				$('#Remarks1').val(data.Remarks1);

				$('.modal-title').text("Edit Student Offense");
				$('#id').val(id);
				$('#action').val("Save Changes");
				$('#operation').val("Edit");
			}
		})
	});
	
	$(document).on('click', '.delete', function(){
		var id = $(this).attr("id");
		if(confirm("Are you sure you want to delete this?"))
		{
			$.ajax({
				url:"deleteshsoff.php",
				method:"POST",
				data:{id:id},
				success:function(data)
				{
					alert(data);
					dataTable.ajax.reload();
				}
			});
		}
		else
		{
			return false;	
		}
	});
	
	
});
</script>