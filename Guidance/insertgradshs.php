<?php
error_reporting(0);
include('db.php');
include('functiongradshs.php');
if(isset($_POST["operation"]))
{
	if($_POST["operation"] == "Edit")
	{
		$statement = $connection->prepare(
			"UPDATE tbl_seniorhigh 
			SET 
				Subject1 = :Subject1,
				Grade1 = :Grade1,

				Subject2 = :Subject2,
				Grade2 = :Grade2,

				Subject3 = :Subject3,
				Grade3 = :Grade3,

				Subject4 = :Subject4,
				Grade4 = :Grade4,

				Subject5 = :Subject5,
				Grade5 = :Grade5,

				Subject6= :Subject6,
				Grade6 = :Grade6,

				Subject7= :Subject7,
				Grade7 = :Grade7,
				
				Subject8= :Subject8,
				Grade8 = :Grade8,

				Subject9= :Subject9,
				Grade9 = :Grade9,

				Subject10= :Subject10,
				Grade10 = :Grade10,

				Subject11 = :Subject11,
				Grade11 = :Grade11,

				Subject12 = :Subject12,
				Grade12 = :Grade12,

				Subject13 = :Subject13,
				Grade13 = :Grade13,

				Subject14 = :Subject14,
				Grade14 = :Grade14,

				Subject15 = :Subject15,
				Grade15 = :Grade15,

				Subject16= :Subject16,
				Grade16 = :Grade16,

				Subject17= :Subject17,
				Grade17 = :Grade17,
				
				Subject18= :Subject18,
				Grade18 = :Grade18,

				Subject19= :Subject19,
				Grade19 = :Grade19,

				Subject20= :Subject20,
				Grade20 = :Grade20,

				Subject21 = :Subject21,
				Grade21 = :Grade21,

				Subject22 = :Subject22,
				Grade22 = :Grade22,

				Subject23 = :Subject23,
				Grade23 = :Grade23,

				Subject24 = :Subject24,
				Grade24 = :Grade24,

				Subject25 = :Subject25,
				Grade25 = :Grade25,

				Subject26= :Subject26,
				Grade26 = :Grade26,

				Subject27= :Subject27,
				Grade27 = :Grade27,
				
				Subject28= :Subject28,
				Grade28 = :Grade28,

				Subject29= :Subject29,
				Grade29 = :Grade29,

				Subject30= :Subject30,
				Grade30 = :Grade30,

				Subject31= :Subject31,
				Grade31 = :Grade31,

				Subject32= :Subject32,
				Grade32 = :Grade32
			WHERE id = :id
			"
		);
		$result = $statement->execute(
			array(
				':Subject1'			=>	$_POST["Subject1"],
				':Grade1'			=>	$_POST["Grade1"],

				':Subject2'			=>	$_POST["Subject2"],
				':Grade2'			=>	$_POST["Grade2"],

				':Subject3'			=>	$_POST["Subject3"],
				':Grade3'			=>	$_POST["Grade3"],

				':Subject4'			=>	$_POST["Subject4"],
				':Grade4'			=>	$_POST["Grade4"],

				':Subject5'			=>	$_POST["Subject5"],
				':Grade5'			=>	$_POST["Grade5"],

				':Subject6'			=>	$_POST["Subject6"],
				':Grade6'			=>	$_POST["Grade6"],

				':Subject7'			=>	$_POST["Subject7"],
				':Grade7'			=>	$_POST["Grade7"],

				':Subject8'			=>	$_POST["Subject8"],
				':Grade8'			=>	$_POST["Grade8"],

				':Subject9'			=>	$_POST["Subject9"],
				':Grade9'			=>	$_POST["Grade9"],

				':Subject10'		=>	$_POST["Subject10"],
				':Grade10'			=>	$_POST["Grade10"],

				':Subject11'		=>	$_POST["Subject11"],
				':Grade11'			=>	$_POST["Grade11"],

				':Subject12'		=>	$_POST["Subject12"],
				':Grade12'			=>	$_POST["Grade12"],

				':Subject13'		=>	$_POST["Subject13"],
				':Grade13'			=>	$_POST["Grade13"],

				':Subject14'		=>	$_POST["Subject14"],
				':Grade14'			=>	$_POST["Grade14"],

				':Subject15'		=>	$_POST["Subject15"],
				':Grade15'			=>	$_POST["Grade15"],

				':Subject16'		=>	$_POST["Subject16"],
				':Grade16'			=>	$_POST["Grade16"],

				':Subject17'		=>	$_POST["Subject17"],
				':Grade17'			=>	$_POST["Grade17"],

				':Subject18'		=>	$_POST["Subject18"],
				':Grade18'			=>	$_POST["Grade18"],

				':Subject19'		=>	$_POST["Subject19"],
				':Grade19'			=>	$_POST["Grade19"],

				':Subject20'		=>	$_POST["Subject20"],
				':Grade20'			=>	$_POST["Grade20"],

				':Subject21'		=>	$_POST["Subject21"],
				':Grade21'			=>	$_POST["Grade21"],

				':Subject22'		=>	$_POST["Subject22"],
				':Grade22'			=>	$_POST["Grade22"],

				':Subject23'		=>	$_POST["Subject23"],
				':Grade23'			=>	$_POST["Grade23"],

				':Subject24'		=>	$_POST["Subject24"],
				':Grade24'			=>	$_POST["Grade24"],

				':Subject25'		=>	$_POST["Subject25"],
				':Grade25'			=>	$_POST["Grade25"],

				':Subject26'		=>	$_POST["Subject26"],
				':Grade26'			=>	$_POST["Grade26"],

				':Subject27'		=>	$_POST["Subject27"],
				':Grade27'			=>	$_POST["Grade27"],

				':Subject28'		=>	$_POST["Subject28"],
				':Grade28'			=>	$_POST["Grade28"],

				':Subject29'		=>	$_POST["Subject29"],
				':Grade29'			=>	$_POST["Grade29"],

				':Subject30'		=>	$_POST["Subject30"],
				':Grade30'			=>	$_POST["Grade30"],

				':Subject31'		=>	$_POST["Subject31"],
				':Grade31'			=>	$_POST["Grade31"],

				':Subject32'		=>	$_POST["Subject32"],
				':Grade32'			=>	$_POST["Grade32"],

				':id'				=>	$_POST["id"]
			)
		);
		if(!empty($result))
		{
			echo 'Grades Updated';
		}
	}
}
?>