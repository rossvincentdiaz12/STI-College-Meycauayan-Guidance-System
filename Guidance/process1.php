<?php

$mysqli = new mysqli('localhost', 'root', '', 'guidance_db');

if (mysqli_connect_errno()) {
  echo json_encode(array('mysqli' => 'Failed to connect to MySQL: ' . mysqli_connect_error()));
  exit;
}

$page = isset($_GET['p'])? $_GET['p'] : '' ;
if($page=='view'){
    $result = $mysqli->query("SELECT * FROM tbl_users WHERE deleted != '1'");
    while($row = $result->fetch_assoc()){
        ?>
        <tr>
            <td><?php echo $row['id'] ?></td>
            <td><?php echo $row['Firstname'] ?></td>
            <td><?php echo $row['Lastname'] ?></td>
            <td><?php echo $row['Username'] ?></td>
            <td><?php echo $row['Password'] ?></td>
        </tr>
        <?php
    }
} else{

    // Basic example of PHP script to handle with jQuery-Tabledit plug-in.
    // Note that is just an example. Should take precautions such as filtering the input data.

    header('Content-Type: application/json');

    $input = filter_input_array(INPUT_POST);



    if ($input['action'] == 'edit') {
        $mysqli->query("UPDATE tbl_users SET id='" . $input['id'] . "', Firstname='" . $input['Firstname'] . "', Lastname='" . $input['Lastname'] . "', Username='" . $input['Username'] . "', Password='" . $input['Password'] . "' WHERE id='" . $input['id'] . "'");
    } else if ($input['action'] == 'delete') {
        $mysqli->query("UPDATE tbl_users SET deleted=1 WHERE id='" . $input['id'] . "'");
    } else if ($input['action'] == 'restore') {
        $mysqli->query("UPDATE tbl_users SET deleted=0 WHERE id='" . $input['id'] . "'");
    }

    mysqli_close($mysqli);

    echo json_encode($input);
    
}
?>