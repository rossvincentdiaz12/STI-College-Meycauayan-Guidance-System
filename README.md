# STI College Meycauayan Guidance System

STI College Meycauayan Guidance System

Live Demo : http://stiguidance.c1.biz/

Sample admin: 
* user:admin
* pass:admin

## Getting Started
Install Xampp or Wampp on your computer
Install SQLyog etc. or PhpmyAdmin for database


### Prerequisites

Template for csvfile

### Installing
open you SQLyog or PhpmyAdmin and import the database db.sql
Install your xampp to you computer the put the file into htdocs folder


## Deployment

run your xampp controll panel and click start for apache and sql

open your browser then type localhost/*name of the folder*
## Built With

* [BOOSTRAP](https://getbootstrap.com/docs/4.1/getting-started/introduction/) 
* [PHP](https://www.php.net/docs.php) 

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details