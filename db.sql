/*
SQLyog Ultimate v10.00 Beta1
MySQL - 5.5.5-10.4.11-MariaDB : Database - guidance_db
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`guidance_db` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `guidance_db`;

/*Table structure for table `tbl_announcements` */

DROP TABLE IF EXISTS `tbl_announcements`;

CREATE TABLE `tbl_announcements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `AnnouncementDate` varchar(100) DEFAULT NULL,
  `Title` varchar(200) DEFAULT NULL,
  `Details` varchar(9999) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_announcements` */

insert  into `tbl_announcements`(`id`,`AnnouncementDate`,`Title`,`Details`) values (2,'October 7, 2018','Suspensionss','In lieu of the previous announcement regarding the classes for today, September 18, 2018, our Security Guard has reported that Meralco has resume our regular electricity supply already.  We can pursue with our REGULAR CLASSES for today.  Since we have suspended the morning classes (7 am), you are excuse from the 1st class.  Our apology for the confusion it may cause you.  Thank you.');

/*Table structure for table `tbl_offenses` */

DROP TABLE IF EXISTS `tbl_offenses`;

CREATE TABLE `tbl_offenses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Offenses` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_offenses` */

insert  into `tbl_offenses`(`id`,`Offenses`) values (4,'cheating'),(5,'asdasd'),(6,'dsadasdsadsadsa'),(7,'asdsadasd'),(8,'cheaters');

/*Table structure for table `tbl_users` */

DROP TABLE IF EXISTS `tbl_users`;

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Firstname` varchar(50) NOT NULL,
  `Lastname` varchar(50) NOT NULL,
  `Username` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `ConfirmPass` varchar(50) NOT NULL,
  `UserType` enum('master','user') NOT NULL,
  `UserStatus` enum('Active','Inactive') NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_users` */

insert  into `tbl_users`(`id`,`Firstname`,`Lastname`,`Username`,`Password`,`ConfirmPass`,`UserType`,`UserStatus`) values (1,'Ross Vincent','Diaz','admin','admin','admin','master','Active');

/*Table structure for table `user_details` */

DROP TABLE IF EXISTS `user_details`;

CREATE TABLE `user_details` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_email` varchar(200) NOT NULL,
  `user_password` varchar(200) NOT NULL,
  `user_name` varchar(200) NOT NULL,
  `user_type` enum('master','user') NOT NULL,
  `user_image` varchar(150) NOT NULL,
  `user_status` enum('Active','Inactive') NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `user_details` */

insert  into `user_details`(`user_id`,`user_email`,`user_password`,`user_name`,`user_type`,`user_image`,`user_status`) values (1,'john_smith@gmail.com','$2y$10$cHpf3TzonURXDENRiRF0de1ycSfnM4NJ27sdwyUCf5L.sewDlkCBe','John Smith','master','john_smith.jpg','Active'),(2,'dona_huber@gmail.com','$2y$10$lcLYyNeK1adgzYcBplv45uuXHFuFyWYThnj3nB2SZ/LbQvtWSoGjO','Dona Huber','user','dona_huber.jpg','Active'),(3,'roy_hise@gmail.com','$2y$10$XlyVI9an5B6rHW3SS9vQpesJssKJxzMQYPbSaR7dnpWjDI5fpxJSS','Roy Hise','user','roy_hise.jpg','Active'),(4,'peter_goad@gmail.com','$2y$10$n1B.FdHNwufTkmzp/pNqc.EiwjB8quQ1tBCEC7nkaldI5pS.et04e','Peter Goad','user','peter_goad.jpg','Active'),(5,'sarah_thomas@gmail.com','$2y$10$s57SErOPlgkIZf1lxzlX3.hMt8LSSKaYig5rusxghDm7LW8RtQc/W','Sarah Thomas','user','sarah_thomas.jpg','Active'),(6,'edna_william@gmail.com','$2y$10$mfMXnH.TCmg5tlYRhqjxu.ILly8s9.qsLKOpyxgUl6h1fZt6x/B5C','Edna William','user','edna_william.jpg','Active'),(7,'admin','admin','asdasd','master','','Active');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
